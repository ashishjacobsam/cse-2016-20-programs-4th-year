#include<iostream>
#include "../../Custom-DSA/DisjointSet.cpp"
#include "../../Custom-DSA/UWGraph.cpp"
using namespace std;
uint KruskalMST(UWGraph g)
{
	register uint v=g.VerticesCount(),e=g.EdgesCount(),i,j=0,k=0;
	uint **edges=new uint*[e];
	UWEdge *node=g.GetHead();//Step 1: Get all the Edges
	while(node!=nullptr)
	{
		edges[j]=new uint[3];
		edges[j][0]=node->Vertex1;
		edges[j][1]=node->Vertex2;
		edges[j++][2]=node->weight;
		node=node->Next;
	}
	e--;
	for(i=0;i<e;i++)//Step 2: Sort the edges in ascending order of their weights
	{
		for(j=i+1;j<=e;j++)
		{
			if(edges[i][2]>edges[j][2])//Swap
			{
				k=edges[i][0];
				edges[i][0]=edges[j][0];
				edges[j][0]=k;
				k=edges[i][1];
				edges[i][1]=edges[j][1];
				edges[j][1]=k;
				k=edges[i][2];
				edges[i][2]=edges[j][2];
				edges[j][2]=k;
			}
		}
	}
	e++;
	k=0;
	DisjointSets tree(v);//This is for checking Cycles and stuff
	for(i=1,j=0;j<e&&i<v;j++)//We need to pick (V-1) edges to form a tree
	{
		if(tree.GetRoot(edges[j][0])!=tree.GetRoot(edges[j][1]))
		{
			k+=edges[j][2];
			i++;
			tree.Union(edges[j][0],edges[j][1]);
		}
	}
	if(i<v)return 0;
	else return k;
}
int main()
{
	uint v,e,p,q,w;
	cout<<"Enter Vertices and Edges of the graph: ";
	cin>>v>>e;
	UWGraph g(v);
	cout<<"Enter "<<e<<" Edges\n";
	while(e-->0)//While e tends to 0
	{
		cin>>p>>q>>w;
		g.SetEdge(p,q,w);
	}
	e=KruskalMST(g);
	if(e)cout<<"The Minimum Spanning Tree of the given graph has cost = "<<e;
	else cout<<"The graph is not connected. The MST cannot be formed.";
}