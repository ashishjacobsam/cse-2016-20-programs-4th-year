#include<stdio.h>//Because for **SOME** people, "using namespace std" can be a pain.
#include<iostream>//I am using  new operator for allocation. To do this completly in C, use malloc() or statically defined large array
struct Job
{
	unsigned int Profit;
	unsigned int Deadline;
	unsigned int ID;
};
struct Job* SortJobs(unsigned int jobs,struct Job *Jobs)
{
	register unsigned int i,j;
	jobs--;
	for(i=0;i<jobs;i++)
	{
		for(j=i+1;j<=jobs;j++)
		{
			if(Jobs[i].Profit<Jobs[j].Profit)
			{
				struct Job temp=Jobs[i];
				Jobs[i]=Jobs[j];
				Jobs[j]=temp;
			}
		}
	}
	return Jobs;
}
unsigned int* JSwSD(unsigned int jobs,struct Job *Jobs)//Job Sequencing with Specified Deadlines
{
	Jobs=SortJobs(jobs,Jobs);
	unsigned int *TimeSlot=new unsigned int[jobs],i,j,k;
	for(i=0;i<jobs;i++)TimeSlot[i]=0;
	for(i=0;i<jobs;i++)
	{
		k=Jobs[i].Deadline-1;
		if(TimeSlot[k]==0)TimeSlot[k]=Jobs[i].ID;
		else
		{
			while(k>=0&&TimeSlot[k]!=0)k--;
			if(k>=0)TimeSlot[k]=Jobs[i].ID;
		}
	}
	return TimeSlot;
}
int main()
{
	unsigned int j,i,*res;
	printf("Enter the no. of jobs to schedule :");
	scanf("%d",&j);
	struct Job *Js=new Job[j];
	for(i=0;i<j;i++)
	{
		printf("Enter Profit and deadline for Job ID J%d :",(i+1));
		Js[i].ID=i+1;
		scanf("%d %d",&Js[i].Profit,&Js[i].Deadline);
	}
	res=JSwSD(j,Js);
	int profit=0;
	printf("\nOptimized Schedule :");
	for(int i=0;i<j;i++)
	{
		if(res[i]!=0)//If result is 0, it means no job declared at this instant
		{
			printf("J%d ",res[i]);//Print the Job ID
			profit+=Js[i].Profit;//Since we called by reference, the Jobs are now in their sorted state
		}
		else printf(" - ");
	}
	printf("\nNet Profit = %d",profit);
	return 0;
}//7 3 1 5 3 20 4 18 3 1 2 6 1 30 2
//6 200 5 180 3 190 3 300 2 120 4 100 2