#include<iostream>
short board[4][4];//ChessBoard of 4x4
//Global variables are by default 0, so no need to make all of them 0 in main()
int isValidToPlace(short row,short col)
{
	register unsigned short i,a,b,c,d;
	for(i=0;i<4;i++)
	{
		if(row!=i&&board[i][col]==1)return 0;//Invalid, since There is already a queen at the same column
		if(col!=i&&board[row][i]==1)return 0;//Invalid, since There is already a queen at the same row
	}//Row and column check complete
	for(i=1;i<4;i++)
	{
		a=row+i;
		b=row-i;
		c=col+i;
		d=col-i;
		if((a<4)&&(c<4)&&(board[a][c]==1))return 0;
		if((a<4)&&(d<4)&&(board[a][d]==1))return 0;
		if((b<4)&&(c<4)&&(board[b][c]==1))return 0;
		if((b<4)&&(d<4)&&(board[b][d]==1))return 0;
	}//Diagonal check complete
	return 1;//Valid
}
int PlaceQueen(short currow, short curcol, short remaining)
{
	if(remaining==0)return 1;
	else
	{
		for(register short i=(currow*4)+curcol;i<16;i++)
		{
			if(isValidToPlace(i/4,i%4))
			{
				board[i/4][i%4]=1;//Make the change
				i++;//Now we need to check for the next square
				if(PlaceQueen(i/4,i%4,remaining-1)==1)return 1;
				i--;//Undo
				board[i/4][i%4]=0;//Undo
			}
		}
	}
}
using namespace std;
void PrintBoard()
{
	register short i,j;
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			cout<<"["<<(board[i][j]==1?"Q":" ")<<"]";
		}
		cout<<"\n";
	}
}
int main()
{
	if(PlaceQueen(0,0,4))PrintBoard();
	else cout<<"Not possible to place 4 Queens in 4x4 Chessboard";
	return 0;
}