#include <iostream>
using namespace std;
void Merge(int *array,int l,int m,int r)
{
	int i,j,k,n1=m-l+1,n2=r-m;
	int *L=new int[n1],*R=new int[n2];
	for(i=l,k=0;i<=m;i++,k++)L[k]=array[i];
	for(i=m+1,k=0;i<=r;i++,k++)R[k]=array[i];
	i=j=0;
	k=l;
	while(i<n1&&j<n2)
	{
		if(L[i]<R[j])array[k++]=L[i++];
		else array[k++]=R[j++];
	}
	while(i<n1)array[k++]=L[i++];
	while(j<n2)array[k++]=R[j++];
}
void MergeSort(int *array, int low, int high)
{
	if(low>=high)return;		//If elements are less than 2, skip everything
	int mid=(low/2)+(high/2);	//Better than (high+low)/2; No chance of overflow
	MergeSort(array,low,mid);
	MergeSort(array,mid+1,high);
	Merge(array,low,mid,high);
}
void MergeSort(int *array, int size) { MergeSort(array, 0, size - 1); }
int main()
{
	int *a, n;
	cout << "Enter Size of array: ";
	cin >> n;
	a = new int[n];
	cout << "Enter " << n << " elements : ";
	for (int i = 0; i < n; i++)
		cin >> a[i];
	MergeSort(a, n);
	cout << "\nSorted array now : ";
	for (int i = 0; i < n; i++)
		cout << a[i] << " ";
	return 0;
}