#include<iostream>
using namespace std;
void Swap(int &a,int &b)
{
	a^=b;
	b^=a;
	a^=b;
}
void SelectionSort(int *a,int size)
{
	for(int i=0;i<size-1;i++)
	{
		int min=i;
		for(int j=i+1;j<size;j++)
			if(a[min]>a[j]) min=j;
		if(min!=i) Swap(a[i],a[min]);
	}
}
int main()
{
	int *a, n;
	cout << "Enter Size of array: ";
	cin >> n;
	a = new int[n];
	cout << "Enter " << n << " elements : ";
	for (int i = 0; i < n; i++)
		cin >> a[i];
	SelectionSort(a, n);
	cout << "\nSorted array now : ";
	for (int i = 0; i < n; i++)
		cout << a[i] << " ";
	return 0;
}