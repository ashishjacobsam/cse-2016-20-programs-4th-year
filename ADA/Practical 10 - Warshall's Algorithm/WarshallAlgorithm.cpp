#include<iostream>
using namespace std;
int infinity=0;
int** WarshallAlgorithm(int **AdjMat,int v)
{
	register int i,j,k;
	for(i=0;i<v;i++)
	{
		for(j=0;j<v;j++)
		{
			if(AdjMat[j][i]==infinity)continue;
			for(k=0;k<v;k++)
			{
				if(AdjMat[i][k]==infinity)continue;
				if(AdjMat[j][k]==infinity||AdjMat[j][k]>AdjMat[j][i]+AdjMat[i][k])
					AdjMat[j][k]=AdjMat[j][i]+AdjMat[i][k];
			}
		}
	}
	return AdjMat;
}
int main()
{
	register int v,e,p,q,w;
	char ud;
	cout<<"Enter Vertices and Edges of the graph: ";
	cin>>v>>e;
	cout<<"Is graph Undirected? (Y/N):";
	cin>>ud;
	if(ud=='y'||ud=='Y')ud=1;
	else ud=0;
	int **x=new int*[v];
	for(p=0;p<v;p++)x[p]=new int[v]();
	cout<<"Enter "<<e<<" Edges\n";
	while(e-->0)//While e tends to 0
	{
		cin>>p>>q;
		cin>>x[p][q];
		if(ud)x[q][p]=x[p][q];//This is an undirected graph
	}
	x=WarshallAlgorithm(x,v);
	cout<<"The All-Pair distance is given as follows\n";
	for(p=0;p<v;p++)cout<<"\t"<<p;
	cout<<"\n";
	for(p=0;p<v;p++)
	{
		cout<<p<<"\t";
		for(q=0;q<v;q++)cout<<x[p][q]<<"\t";
		cout<<"\n";
	}
}