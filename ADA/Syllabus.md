| Sno | Session Plan Header | Session Plan Topic                        | Batch A:Date of Experiment | Batch A:Date of Submission | Batch B:Date of Experiment | Batch B:Date of Submission |
|-----|---------------------|-------------------------------------------|----------------------------|----------------------------|----------------------------|----------------------------|
| 1   | Divide and conquer  | Binary Search                            G| 28-08-2019                 | 04-09-2019                 | 23-08-2019                 | 30-08-2019                 |
| 2   |                     | Quick sort                               G| 28-08-2019                 | 04-09-2019                 | 23-08-2019                 | 30-08-2019                 |
| 3   |                     | Merge sort                               P| 04-09-2019                 | 11-09-2019                 | 30-08-2019                 | 06-09-2019                 |
| 4   |                     | Selection sort                           P| 04-09-2019                 | 11-09-2019                 | 30-08-2019                 | 06-09-2019                 |
| 5   | Greedy Method       | Fractional Knapsack                      P| 11-09-2019                 | 18-09-2019                 | 06-09-2019                 | 13-09-2019                 |
| 6   |                     | Job Sequencing with specified deadlines  G| 11-09-2019                 | 18-09-2019                 | 06-09-2019                 | 13-09-2019                 |
| 7   |                     | Prims Method                             P| 18-09-2019                 | 25-09-2019                 | 13-09-2019                 | 20-09-2019                 |
| 8   |                     | Kruskal Method                           G| 25-09-2019                 | 09-10-2019                 | 13-09-2019                 | 20-09-2019                 |
| 9   | Dynamic Programming | LCS                                      P| 25-09-2019                 | 09-10-2019                 | 20-09-2019                 | 27-09-2019                 |
| 10  |                     | Warshall's algorithm                     G| 09-10-2019                 | 16-10-2019                 | 20-09-2019                 | 27-09-2019                 |
| 11  | Traversal of Graphs | BFS                                      G| 09-10-2019                 | 16-10-2019                 | 27-09-2019                 | 11-10-2019                 |
| 12  |                     | DFS                                      G| 16-10-2019                 | 23-10-2019                 | 27-09-2019                 | 11-10-2019                 |
| 13  | Backtacking         | 4 Queen Problem                          G| 16-10-2019                 | 23-10-2019                 | 11-10-2019                 | 01-11-2019                 |
| 14  | Branch and Bound    | TSP                                      P| 23-10-2019                 | 30-10-2019                 | 11-10-2019                 | 01-11-2019                 |




The practicals marked with 'G' have writeup available in this (Gitlab) repository. The rest are to be taken from the pdf uploaded in the Drive.