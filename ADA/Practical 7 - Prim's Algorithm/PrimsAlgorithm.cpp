#include "..\..\Custom-DSA\ArrayGraph.cpp"
#include<iostream>
using namespace std;
uint PrimMST(ArrayGraph g)
{
	register uint i,j,k,l=g.GetVerticesCount(),cost=0,wmin;
	uint *Dist=new uint[l]();
	short *Reach=new short[l]();//This braces thingy calls the constructor for each element and makes them 0. Neat, isn't it?
	Reach[0]=1;//All Vertices are unreachable except for any one. Here 0th Vertex is chosen
	for(i=0;i<l;i++)//Every pass of this loop includes a single vertex to the tree. We want to involve all our V vertices.
	{
		k=0;
		wmin=0-1;//Since k is unsigned, k is theoretically infinity now, (practically FFFFFFFF = ‭4,29,49,67,295‬  (in 32-bit PCs which have int as 4 byte))
		for(j=0;j<l;j++)
		{
			if(Reach[j]==1&&Dist[j]<wmin)
			{
				wmin=Dist[j];
				k=j;
			}
		}
		if(Reach[k]!=1)return 0;//This is not a complete Graph and MST is not applicable on this!
		Reach[k]=2;//This Vertex is reached and processed.
		cost+=wmin;//This is added to MST's cost
		for(j=0;j<l;j++)//Now, Explore this vertex
		{
			if(Reach[j]!=2)//Now for the vertex who are unreachable, but not for long... 
			{
				wmin=g.Get(k,j);
				if(wmin>0)
				{
					if(Reach[j]==0)
					{
						Dist[j]=wmin;
						Reach[j]=1;
					}
					else if(wmin<Dist[j])//If Edge exists
						Dist[j]=wmin;
				}
			}
		}
	}
	return cost;
}
int main()
{
	uint v,e,p,q,w;
	cout<<"Enter Vertices and Edges of the graph: ";
	cin>>v>>e;
	ArrayGraph g(v);
	cout<<"Enter "<<e<<" Edges\n";
	while(e-->0)//While e tends to 0
	{
		cin>>p>>q>>w;
		g.Set(p,q,w);
		g.Set(q,p,w);//This is an undirected graph
	}
	e=PrimMST(g);
	if(e)cout<<"The Minimum Spanning Tree of the given graph has cost = "<<e;
	else cout<<"The graph is not connected. The MST cannot be formed.";
	return 0;
}