#include<iostream>
using namespace std;
struct KnapObject
{
	int id;
	int Profit;
	int Weight;
};
KnapObject* Sort(KnapObject *obj,int n)
{
	n--;
	for(int i=0;i<n;i++)
	{
		for(int j=i+1;j<=n;j++)
		{
			if(obj[i].Profit*obj[j].Weight<obj[j].Profit*obj[i].Weight)
			{
				KnapObject temp=obj[i];
				obj[i]=obj[j];
				obj[j]=temp;
			}
		}
	}
	return obj;
}
float *FractionalKnapsack(int M,int n,int *P,int *W)
{
	float *solution=new float[n]();
	KnapObject *Objects=new KnapObject[n];
	for(int i=0;i<n;i++)
	{
		Objects[i].id=i;
		Objects[i].Profit=P[i];
		Objects[i].Weight=W[i];
	}
	Objects=Sort(Objects,n);
	int u=M,i,j;
	for(i=0;u>W[Objects[i].id]&&i<n;i++)
	{
		j=Objects[i].id;
		solution[j]=1;
		u=u-W[j];
	}
	if(i<n)solution[Objects[i].id]=u/(float)W[Objects[i].id];
	return solution;
}
int main()
{
	int n,M,*P,*W;
	float *sol;
	cout<<"Enter the Size of Knapsack: ";
	cin>>M;
	cout<<"Enter the number of available objects: ";
	cin>>n;
	P=new int[n];
	W=new int[n];
	cout<<"Enter the Profit vector from 0 to "<<n-1<<": ";
	for(int i=0;i<n;i++)cin>>P[i];
	cout<<"Enter the Weight vector from 0 to "<<n-1<<": ";
	for(int i=0;i<n;i++)cin>>W[i];
	sol=FractionalKnapsack(M,n,P,W);
	cout<<"\nThe weight vector is : [";
	float Profit=0;
	for(int i=0;i<n;i++)
	{
		cout<<sol[i]<<((i!=n-1)?", ":"");//The extra fluff is there just to insert a comma if it is not the last element
		Profit+=sol[i]*P[i];
	}
	cout<<"]\nThe net profit is "<<Profit;
	return 0;
}
//20 3 25 24 15 18 15 10