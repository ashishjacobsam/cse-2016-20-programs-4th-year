#include<iostream>
using namespace std;
typedef unsigned int uint;
uint** CloneMatrix(uint **Mat, uint v)
{
	uint **clone=new uint*[v];
	for(register int i=0,j;i<v;i++)
	{
		clone[i]=new uint[v];
		for(j=0;j<v;j++)clone[i][j]=Mat[i][j];
	}
	return clone;
}
unsigned short FullyReduced(uint **AdjMat, uint**InfMat, uint v)
{
	register uint i,j,k,cost=0;
	for(i=0;i<v;i++)
	{
		k=0;//K is the index of the smallest element
		for(j=1;j<v;j++)k=(((InfMat[i][k]==0)&&(AdjMat[i][k]>AdjMat[i][j]))||(InfMat[i][k]==1))&&(InfMat[i][j]==0)?j:k;
		if(InfMat[i][k]==0&&AdjMat[i][k]!=0)
		{
			k=AdjMat[i][k];
			cost+=k;
			for(j=0;j<v;j++)AdjMat[i][j]-=InfMat[i][j]==0?k:0;
		}
	}
	for(i=0;i<v;i++)
	{
		k=0;//K is the index of the smallest element
		for(j=1;j<v;j++)k=(((InfMat[k][i]==0)&&(AdjMat[k][i]>AdjMat[j][i]))||(InfMat[k][i]==1))&&(InfMat[j][i]==0)?j:k;
		if(InfMat[k][i]==0&&AdjMat[k][i]!=0)
		{
			k=AdjMat[k][i];
			cost+=k;
			for(j=0;j<v;j++)AdjMat[j][i]-=InfMat[j][i]==0?k:0;
		}
	}
	return cost;
}
uint* TSP(uint **OAdjMat, uint**OInfMat, uint v,uint &cost)
{
	uint i,j,k,w,PathLength=1;
	uint *path=new uint[v],*done=new uint[v](),**AdjMat=CloneMatrix(OAdjMat,v),**InfMat=CloneMatrix(OInfMat,v);
	path[0]=0;
	done[0]=1;
	cost=FullyReduced(AdjMat,InfMat,v);
	uint **MinAdjMat=CloneMatrix(AdjMat,v),**MinInfMat=CloneMatrix(InfMat,v),Val;
	do
	{
		Val=-1;w=v;
		uint **iAdjMat=nullptr,**iInfMat=nullptr,iVal;
		k=path[PathLength-1];//The last vertex visited
		for(i=1;i<v;i++)
		{
			if(done[i]==0&&InfMat[k][i]==0)
			{
				iAdjMat=CloneMatrix(AdjMat,v);
				iInfMat=CloneMatrix(InfMat,v);
				iInfMat[i][0]=1;//Set Adj[i,0]=infinity
				for(j=0;j<v;j++)iInfMat[k][j]=iInfMat[j][i]=1;//Set kth row and ith collumn as infinite
				iVal=FullyReduced(iAdjMat,iInfMat,v);
				iVal+=AdjMat[k][i];
				if(iVal<Val||Val==-1)
				{
					Val=iVal;
					w=i;
					MinAdjMat=iAdjMat;
					MinInfMat=iInfMat;
				}
			}
		}
		path[PathLength++]=w;
		done[w]=1;
		cost+=Val;
		AdjMat=MinAdjMat;
		InfMat=MinInfMat;
	}while(PathLength<v);
	return path;
}
void Print(uint **ad,uint**inf,uint v)
{
	for(register uint x=0,y;x<v;x++)
	{
		for(y=0;y<v;y++)
		{
			if(inf[x][y]==1)cout<<"&\t";
			else cout<<ad[x][y]<<"\t";
		}
		cout<<"\n";
	}
}
int main()
{
	register uint v,e,p,q;
	char ud;
	cout<<"Enter the count of Vertices in graph: ";cin>>v;
	cout<<"Is graph Undirected? (Y/N): ";cin>>ud;
	cout<<"Enter adjacency Matrix. [Enter 0 if no direct edge exists]\n";
	uint **AdjMat=new uint*[v];
	uint **infMat=new uint*[v];
	for(p=0;p<v;p++)
	{
		AdjMat[p]=new uint[v]();
		infMat[p]=new uint[v]();
		infMat[p][p]=1;
	}
	if(ud=='y'||ud=='Y')
	{
		for(p=0;p<v;p++)
		{
			for(q=0;q<p;q++)
			{
				cout<<"Enter weight of edge ("<<p<<", "<<q<<") :";
				cin>>AdjMat[p][q];
				AdjMat[q][p]=AdjMat[p][q];
				if(AdjMat[p][q]==0)infMat[p][q]=infMat[q][p]=1;
			}
		}
	}
	else
	{
		for(p=0;p<v;p++)
		{
			for(q=0;q<v;q++)
			{
				if(p!=q)
				{
					cout<<"Enter weight of edge ("<<p<<", "<<q<<") :";
					cin>>AdjMat[p][q];
					if(AdjMat[p][q]==0)infMat[p][q]=1;
				}
			}
		}
	}
	Print(AdjMat,infMat,v);
	uint *x=TSP(AdjMat,infMat,v,p);
	cout<<"Total Distance : "<<p<<"\n";
	for(p=0;p<v;p++)cout<<x[p]<<" -> ";
	cout<<x[0];
	return 0;
}
/*
4
n
4
12
7
5
0
18
11
0
6
10
2
3
*/


/*
5
y
10
8
10
9
5
8
7
6
9
6
*/