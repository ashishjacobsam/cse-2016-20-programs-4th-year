#include<iostream>
#define Max(x,y) x>y?x:y;
using namespace std;
int strlength(char str[])
{
	int i=0;
	char* x=str;
	while(*(x++))i++;
	return i;
}
int **D;
void Print(char a[],int n,char b[],int m)
{
	register int i,j;
	cout<<"\n\t";
	for(i=0;i<m;i++)cout<<b[i]<<"\t";
	cout<<"\n";
	for(i=1;i<=n;i++)
	{
		for(j=0;j<=m;j++)
		{
			if(j==0)cout<<a[i-1]<<"\t";
			else cout<<D[i][j]<<"\t";
		}
		cout<<"\n";
	}
}
void Construct(char a[],int n,char b[],int m)
{
	register int i,j;
	D=new int*[n+1];
	for(i=0;i<=n;i++)
	{
		D[i]=new int[m+1];
		D[i][0]=0;
	}
	for(j=0;j<=m;j++)D[0][j]=0;
	for(i=1;i<=n;i++)
	{
		for(j=1;j<=m;j++)
		{
			D[i][j]=(a[i-1]==b[j-1])?D[i-1][j-1]+1:Max(D[i-1][j],D[i][j-1]);
		}
	}
	Print(a,n,b,m);//Just for fun. Comment it if you don't want to print it
}
int LCS(char a[],char b[],char *&sol)
{
	register int i,n=strlength(a),m=strlength(b);
	Construct(a,n,b,m);
	int length=D[n--][m--];
	sol=new char[1+length]();
	sol[length]='\0';
	i=length-1;
	while(i>=0)
	{
		if(a[n]==b[m])
		{
			sol[i--]=a[n--];
			m--;
		}
		else if(D[n-1][m]>D[n][m-1])n--;
		else m--;
	}
	return length;
}
int main()
{
	char a[100],b[100],*ans;
	cout<<"Enter string a and b \n";
	cin>>a>>b;
	int result=LCS(a,b,ans);
	cout<<"\nLargest possible subsequence length : "<<result;
	if(result)cout<<"\nOne possible subsequence is :"<<ans;
}