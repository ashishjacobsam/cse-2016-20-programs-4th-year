#include<iostream>
#include "..\BinarySearch.cpp"
struct TestCaseArgument					//Custom Testcase structure for BinarySearch
{
	int *array;
	int size,key,expected;
	int Verify()
	{
		if(BinarySearch(array,size,key)==expected)return 1;
		else return 0;
	}
	TestCaseArgument(){}
	TestCaseArgument(int *x,int s,int k,int e)
	{
		array=x;
		size=s;
		key=k;
		expected=e;
	}
};
int main(char* argc,int val)			//Since we have included BinarySearch.cpp with its own main function, This won't compile.
{										//So to run it, you will need to change the funciton there (Temorary) so that this function is not overridden.
	cout<<"Running the Testcase Now :";
	TestCaseArgument args[]=
	{
		{new int[10]{1,2,5,7,11,13,16,21,23,35},10,11,4},
		{new int[10]{1,2,5,7,11,13,16,21,23,35},10,12,-1},
		{new int[10]{12,35,41,56,75,88,101,121,153,176},10,101,6},
		{new int[8]{21,33,52,63,98,103,153,210},8,210,7},
		{new int[30]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30},30,22,21}
	};
	int size=5,pass=0,fail=0,res;
	for(int i=0;i<size;i++)
	{
		if(args[i].Verify()){pass++;cout<<"\nTest Case "<<i<<" Passed";}
		else {fail++;cout<<"\nTest Case "<<i<<" Failed";}
	}
	cout<<"\n\n---------------------------\nTotal : "<<size<<"\nPassed :"<<pass<<"\nFailed :"<<fail;
}