#include<iostream>
using namespace std;
int BinarySearch(int *a,int n,int key)	//The method to test
{
	int low=0,high=n-1,mid,val;
	while(low<=high)
	{
		mid=(low+high)/2;
		val=a[mid];						//The element to compare
		if(val==key)return mid;			//Element Found
		else if(val<key)low=mid+1;		//Element is larger, therefore we need to search at the right half of array
		else high=mid-1;				//Element is smaller, so we need to search at the lower half of array
	}
	return -1;							//Could not find element
}
void sort(int *a,int n)					//Sorting of numbers
{										//Necessary to sort before using Binary search
	for(int i=0;i<n-1;i++)
	{
		for(int j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				a[i]=a[i]^a[j];			//Swap Logic
				a[j]=a[i]^a[j];			//Using XOR gates
				a[i]=a[i]^a[j];			//Easier to remember
			}
		}
	}
}
int main()
{
	int n,*a,k;
	cout<<"Enter an array size: ";
	cin>>n;
	a=new int[n];
	cout<<"Enter "<<n<<" Elements :";
	for(int i=0;i<n;i++)cin>>a[i];
	sort(a,n);
	cout<<"Sorted array now becomes :";
	for(int i=0;i<n;i++)cout<<a[i]<<" ";
	cout<<"\nEnter the element to search :";
	cin>>k;
	int ans=BinarySearch(a,n,k);
	if(ans>=0)cout<<"Element found at index "<<ans;
	else cout<<"Element not found";
	return 0;
}