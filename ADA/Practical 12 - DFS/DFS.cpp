#include<iostream>
#include "..\..\Custom-DSA\BitArrayGraph.cpp"
using namespace std;
struct Node
{
	uint val;
	Node *Next;
	Node(uint i){val=i;Next=nullptr;}
};
uint* DFS(Graph g,uint Start)
{
	register uint v=g.GetVerticesCount(),i,j,k;
	uint *Pred=new uint[v];
	for(i=0;i<v;i++)Pred[i]=i;
	short *color=new short[v]();
	color[Start]=1;
	Node *Stack=new Node(Start),*inode;
	do
	{
		inode=Stack;
		Stack=Stack->Next;
		i=inode->val;//This is the node to explore
		delete inode;
		for(j=0;j<v;j++)
		{
			if(i==j)continue;
			else if(g.Get(i,j)&&color[j]==0)//If edge exists between i and j
			{
				inode=new Node(j);
				inode->Next=Stack;
				Stack=inode;//Add this to stack
				color[j]=1;//This node is no longer unvisited
				Pred[j]=i;//To reach node j, goto i 
			}
		}
	} while (Stack!=nullptr);
	return Pred;
}
int main()
{
	register uint v,e,p,q;
	char ud;
	cout<<"Enter Vertices and Edges of the graph: ";cin>>v>>e;
	cout<<"Is graph Undirected? (Y/N):";cin>>ud;
	if(ud=='y'||ud=='Y')ud=1;else ud=0;
	Graph x(v);
	cout<<"Enter "<<e<<" Edges\n";
	while(e-->0)//While e tends to 0
	{
		cin>>p>>q;
		x.Set(p,q);
		if(ud)x.Set(q,p);//This is an undirected graph
	}
	uint *Pred=DFS(x,0);
	cout<<"\nThe Predecessor array is :-1";
	for(e=1;e<v;e++)cout<<", "<<((Pred[e]==e)?-1:Pred[e]);
	return 0;
}
/*
16 18 y
0 1
1 2
2 11
2 10
1 3
3 12
3 4
4 13
4 5
5 6
5 9
9 15
9 7
7 6
7 8
8 14
8 0
6 0
*/