#include <iostream>
using namespace std;
void swap(int *a, int i, int j)
{
	if (i != j)
	{
		a[i] = a[i] ^ a[j];
		a[j] = a[i] ^ a[j];
		a[i] = a[i] ^ a[j];
	}
}
int partition(int *a, int low, int high)
{
	int i, j; //We take pivot=A[high]
	for (i = j = low; j < high; j++)
	{
		if (a[j] < a[high])
			swap(a, i++, j);
	}
	swap(a, i, high);
	return i;
}
void QuickSort(int *a, int low, int high)
{
	if (high - low >= 1) //No need to sort if less than 2 elements present
	{
		int j = partition(a, low, high);
		QuickSort(a, low, j - 1);
		QuickSort(a, j + 1, high);
	}
}
int main()
{
	int *a, n;
	cout << "Enter Size of array: ";
	cin >> n;
	a = new int[n];
	cout << "Enter " << n << " elements : ";
	for (int i = 0; i < n; i++)
		cin >> a[i];
	QuickSort(a, 0, n - 1);
	cout << "\nSorted array now : ";
	for (int i = 0; i < n; i++)
		cout << a[i] << " ";
	return 0;
}