from datetime import date
import difflib;
class AiQues:
	def __init__(self, arg1, arg2):
		self.queslist=arg1
		self.response=arg2
	def DefaultCheck(self, parameter_list):
		if parameter_list in self.queslist:
			print(self.response)
			return True
		return False
	def StrictCheck(self, parameter_list,slack):
		for x in self.queslist:
			diff=0
			for i,s in enumerate(difflib.ndiff(x, parameter_list)):# This function calculates the difference in two words
				if s[0]=='-':
					diff=diff+1
				elif s[0]=='+':
					diff=diff+1
			if diff < slack:		#If difference is less than the slack I'm giving, Accept it 
				print(self.response)
				return True
		return False
EndQues=AiQues(['END','BYE','GO','EXIT'],"Bye. Nice to meet you.")
QuesList=[AiQues(["HI","HELLO","HEY"],"Hello"),AiQues(["DATE","TODAY"],"Today is "+date.today().strftime("%D-%M-%Y")),AiQues(["THANK"],"You are welcome")]
print("Hello. How may I help you?")
while 1:
	xx=input(">>>").upper().split(" ")
	i=1
	for x in xx:
		if EndQues.DefaultCheck(x):
			exit()
		else:
			for y in QuesList:
				if y.DefaultCheck(x):#Check for exact word
					i=0
	if i==1:		
		for x in xx:
			if EndQues.StrictCheck(x,2):
				exit()
			else:
				for y in QuesList:
						if y.StrictCheck(x,2):#Check with some slack
							i=0
	if i==1:
		print("Sorry. I didn't understand that")
