import random
'''
Custom rules of this Game.
	*	nxn Board
	*	Any Block can be a)Empty(0), b)Goal(1), c)Worm(2), d)Pit(3), e)Breeze
	*	Exactly one worm 
	*	Exaclty n/2 pits
	*	Breeze present in the adjacent Blocks of pit
	*	The Player can 
	**		Move in 4 directions : Up [Move(0)], Down [Move(1)], Left [Move(2)], Right [Move(3)]
	** 		Shoot an arrow in any of the 4 directions(Limited)
	**		Evaluate the current board for Breeze/Goal  (Implicitly)
	**		Whistle(returns echo for Worm only in the adjacent block)
	*	The Player start at Block (0,0)

	Objective : To reach The Goal state without encountering a Pit or a Worm
'''
n=5#Change N here and affect everything
Board = [[0 for i in range(n)] for j in range(n)]#Creating an empty board of n x n, Initialize with Empty(0)
Visit = [[0 for i in range(n)] for j in range(n)]#Creating an empty board of n x n, Initialize with Empty(0)
Whistle=[[0 for i in range(n)] for j in range(n)]#Creating an empty board of n x n, Initialize with Empty(0)
MoveH = [[0 for i in range(n)] for j in range(n)]#Creating an empty board of n x n, Initialize with Empty(0)
Breeze =[[0 for i in range(n)] for j in range(n)]#Creating an empty board of n x n, Initialize with Empty(0)
Board[0][0]=1 #1=>Player
#now put a worm in random slot
x=0
y=0
def isValid(px,py):
	if(px>=0 and px<n and py>=0 and py<n):
		return True
	return False
while(Board[x][y]!=0):
	x=random.randint(1,n-1)
	y=random.randint(1,n-1)
Board[x][y]=2#2=>Worm is placed as 2
for i in [(x+1,y),(x-1,y),(x,y-1),(x,y+1)]:
	if(isValid(i[0],i[1])):
		Whistle[i[0]][i[1]]=1
while(Board[x][y]!=0):
	x=random.randint(0,n-2)
	y=random.randint(0,n-2)
Board[x][y]=4#4=>Goal is placed as 4
#Now put n pits in random places
for p in range(n//2):#We are doing this n/2 times
	while(Board[x][y]!=0):
		x=random.randint(1,n-2)
		y=random.randint(1,n-2)
	Board[x][y]=3#3=Pits are placed at 3
	for i in [(x+1,y),(x-1,y),(x,y-1),(x,y+1)]:
		if(isValid(i[0],i[1])):
			Breeze[i[0]][i[1]]=1
Board[0][0]=0
ArrowCount=2
def Shoot(xx):
	px=xx[0]
	py=xx[1]
	global ArrowCount
	ArrowCount-=1
	if Board[px][py]==2:
		Board[px][py]=0
		for l in (px,py+1),(px,py-1),(px+1,py),(px-1,py):
			if(isValid(l[0],l[1])):
				Whistle[l[0]][l[1]]=0
x=y=0
print("Welcome to Wumpus World.\nPlayer is placed at (0,0)")	#                       888               888      888                           d8888 888888  .d8888b.  
while(Board[x][y]==0):											#                       888               888      888                          d88888   "88b d88P  Y88b 
	Visit[x][y]=2												#                       888               888      888                         d88P888    888 Y88b.      
	for w in [(x,y+1),(x,y-1),(x+1,y),(x-1,y)]:					#  .d8888b .d88b.   .d88888  .d88b.   .d88888      88888b.  888  888          d88P 888    888  "Y888b.   
		if(isValid(w[0],w[1])):									# d88P"   d88""88b d88" 888 d8P  Y8b d88" 888      888 "88b 888  888         d88P  888    888     "Y88b. 
			if Visit[w[0]][w[1]]==0:							# 888     888  888 888  888 88888888 888  888      888  888 888  888        d88P   888    888       "888 
				Visit[w[0]][w[1]]=1								# Y88b.   Y88..88P Y88b 888 Y8b.     Y88b 888      888 d88P Y88b 888       d8888888888    88P Y88b  d88P 
	if(Breeze[x][y]==1):										#  "Y8888P "Y88P"   "Y88888  "Y8888   "Y88888      88888P"   "Y88888      d88P     888    888  "Y8888P"  
		print("\tThere is a breeze here")						#                                                                888                    .d88P            
		for w in [(x,y+1),(x,y-1),(x+1,y),(x-1,y)]:				#                                                           Y8b d88P                  .d88P"             
			if(isValid(w[0],w[1])):								#                                                            "Y88P"                  888P"               
				if Visit[w[0]][w[1]]==1:
					MoveH[w[0]][w[1]]-=10
	if(Whistle[x][y]==1):
		print("\tThe whistle shows there is a worm nearby")
		for w in [(x,y+1),(x,y-1),(x+1,y),(x-1,y)]:				
			if(isValid(w[0],w[1])):
				if Visit[w[0]][w[1]]==1:
					MoveH[w[0]][w[1]]-=10
	max=-10000
	moveTouple=(x,y)
	if(ArrowCount>0 and Whistle[x][y]):
		temp=0
		tXs=0
		tYs=0
		for (Xs,Ys) in [(x,y+1),(x,y-1),(x+1,y),(x-1,y)]:
			ttemp=0
			if(isValid(Xs,Ys) and Visit[Xs][Ys]==1):							#Shooting heuristic
				for(ox,oy) in [(Xs,Ys+1),(Xs,Ys-1),(Xs+1,Ys),(Xs-1,Ys)]:		#We don't have to traverse. So can be done with Minimax algorithm (depth=1)
					if(isValid(ox,oy) and Visit[ox][oy]==2 and (ox,oy)!=(Xs,Ys)):#Check how sure we are that the worm is here
						if(Whistle[ox][oy]==1):
							ttemp+=100
						else:
							ttemp=-1000
				if(ttemp>temp):
					temp=ttemp
					tXs=Xs
					tYs=Ys
		if(temp>0):
			Shoot((tXs,tYs))
			print("\tPlayer shot an arrow at " + tXs.__str__() + ", " + tYs.__str__())
			MoveH[tXs][tYs]+=15
			if(Whistle[x][y]==0):
				print("\tThe worm was probably killed")
				for tl in (x,y+1),(x,y-1),(x+1,y),(x-1,y):
					if(isValid(tl[0],tl[1]) and Visit[tl[0]][tl[1]]<2):
						MoveH[tl[0]][tl[1]]+=10
			else:
				print("\tThe arrow was fired at the wrong direction")
				for tl in (x,y+1),(x,y-1),(x+1,y),(x-1,y):
					if(isValid(tl[0],tl[1]) and Visit[tl[0]][tl[1]]<2 and (not(tl[0]==Xs and tl[1]==Ys))):
						MoveH[tl[0]][tl[1]]-=10
	max=-10000
	dist=100000
	moveTouple=(x,y)#we need to traverse to a location
	for i in range(n):
		for j in range(n):
			if(Visit[i][j]==1):#This slot is yet to visit
				tempdist=((i-x)*(i-x))+((j-y)*(j-y))
				if(MoveH[i][j]<0 and tempdist==1 and Breeze[x][y]==0 and Whistle[x][y]==0):
					MoveH[i][j]=5
				temp=MoveH[i][j]
				if((temp>max) or (temp==max and tempdist<dist)):#A* algorithm : Selection is on the basis of heuristic as well as the distance
					max=temp
					moveTouple=(i,j)
					dist=tempdist
	dist=((moveTouple[0]-x)*(moveTouple[0]-x))+((moveTouple[1]-y)*(moveTouple[1]-y))
	if(dist!=1):
		Stack=[(x,y)]#Start =(x,y), Target=Adjacent visited box to movetouple
		Visited=[[0 for i in range(n)] for j in range(n)]#Creating an empty board of n x n, Initialize with Empty(0)
		Pred=[[(-1,-1) for i in range(n)] for j in range(n)]#Creating an empty board of n x n, Initialize with Empty(0)
		Pred[x][y]=(-2,-2)
		Target=(-1,-1)
		while(Stack.__len__()>0 and Target==(-1,-1)):
			w=Stack.pop(0)
			for k in (w[0],w[1]+1),(w[0],w[1]-1),(w[0]-1,w[1]),(w[0]+1,w[1]):
				if(isValid(k[0],k[1]) and Visited[k[0]][k[1]]==0 and Visit[k[0]][k[1]]==2):
					Stack.insert(0,k)
					Pred[k[0]][k[1]]=w
					Visited[k[0]][k[1]]=1
					dist=((moveTouple[0]-k[0])*(moveTouple[0]-k[0]))+((moveTouple[1]-k[1])*(moveTouple[1]-k[1]))
					if(dist==1):
						Target=k
			Visited[w[0]][w[1]]=2
		str=" "
		while(Pred[Target[0]][Target[1]]!=(-2,-2)):
			str=", ( "+Target[0].__str__()+", "+Target[1].__str__()+ " )"+str
			Target=Pred[Target[0]][Target[1]]
		str="\tPlayer backtracks to  ( "+x.__str__()+", "+y.__str__()+")"+str
		print(str)
	x=moveTouple[0]
	y=moveTouple[1]
	print("Player is now at "+x.__str__()+", "+y.__str__())
if(Board[x][y]==2):
	print("\tPlayer was killed by worm")
elif(Board[x][y]==3):
	print("\tPlayer falls to his death in the pit")
elif(Board[x][y]==4):
	print("\tPlayer wins! he gets the gold")
else:
	print("Board value = "+(Board[x][y]).__str__())
print("----------------GAME OVER------------------------")