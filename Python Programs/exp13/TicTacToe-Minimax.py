board=[" "," "," "," "," "," "," "," "," "]
winCombos=[[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
infinity=10000
def score():
	nO=nX=0
	for iCombo in winCombos:
		scoreO=scoreX=0
		for ipos in iCombo:
			if board[ipos]=='O' and scoreO!=-1:
				scoreO=scoreO+1
				scoreX=-1
			elif board[ipos]=='X' and scoreX!=-1:
				scoreX=scoreX+1
				scoreO=-1
		if scoreX==3:
			return infinity
		elif scoreO==3:
			return -infinity
		else:
			if scoreX>0:
				nX+=1
			if scoreO>0:
				nO+=1
	return nX-nO
nextmove='O'
othermove='X'
presentscore=0
valid=1
plydepth=4
moves=9
def scoreWithDepth(Aimove,Playermove,depth):
	maxScore = -infinity if Aimove==nextmove else infinity;
	slot=-1#Null
	jslot=score()
	if Aimove=='O':
		jslot=-jslot
	global moves
	if jslot==infinity or jslot==-infinity or depth==0 or moves==0:
		return (slot,jslot) if Aimove==nextmove else (slot,-jslot)
	for islot in {0,1,2,3,4,5,6,7,8}:
		if board[islot]==' ':
			board[islot]=Aimove
			moves-=1
			(jslot,jmax)=scoreWithDepth(Playermove,Aimove,depth-1)
			board[islot]=' '
			moves+=1
			if Aimove==nextmove:
				if jmax>maxScore:
					maxScore=jmax
					slot=islot
			else:
				if jmax<maxScore:
					maxScore=jmax
					slot=islot
	return (slot,maxScore)
def drawBoard():
	print('   |   |')
	print(' ' + board[0] + ' | ' + board[1] + ' | ' + board[2] )
	print('   |   |')
	print('-----------')
	print('   |   |')
	print(' ' + board[3] + ' | ' + board[4] + ' | ' + board[5] )
	print('   |   |')
	print('-----------')
	print('   |   |')
	print(' ' + board[6] + ' | ' + board[7] + ' | ' + board[8] )
	print('   |   |')
i=input("Welcome To Tic-Tac-Toe.\nO Playes first. \n Which side do you want (O or X)? :")
if(i!='O' and i!='X'):
	print("Invalid Choice! ")
	exit();
print("Starting Game...")
drawBoard()
while(moves):
	if(i==nextmove):
		valid=1
		while(valid):
			x=int(input("Enter the slot to put your mark :"))
			if x in {0,1,2,3,4,5,6,7,8} and board[x]==' ':
				board[x]=nextmove
				valid=0
			else:
				print("Invlid input!!! try again")
	else:
		(slot,kkk)=scoreWithDepth(nextmove,othermove,plydepth)
		print("AI has put his mark on "+slot.__str__())#We do not need this here. Just for debug purpose
		board[slot]=nextmove
	moves=moves-1
	if nextmove=='O':
		nextmove='X'
		othermove='O'
	else: 
		nextmove='O'
		othermove='X'
	iscore=score()
	drawBoard()
	if(iscore==infinity or iscore==-infinity):
		print("-----------------------\nGAME OVER !!!\n--------------------")
		if(iscore==infinity):
			print("X Player Wins!")
		else:
			print("O Player Wins!")
		exit()
print("It is a Draw! Game Over!")