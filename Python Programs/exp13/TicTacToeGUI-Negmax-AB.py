#There are many GUI options in Python. AppJar was minimalistic so that's why it is used here.
from appJar import gui
import random
#You need to install appjar. From CLI, use command "python -m pip install appjar" as Administrator
board=[" "," "," "," "," "," "," "," "," "]
winCombos=[[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
infinity=10000
nextMove='O'
othermove='X'
def score():
	nO=nX=0
	for iCombo in winCombos:
		scoreO=scoreX=0
		for ipos in iCombo:
			if board[ipos]=='O' and scoreO!=-1:
				scoreO=scoreO+1
				scoreX=-1
			elif board[ipos]=='X' and scoreX!=-1:
				scoreX=scoreX+1
				scoreO=-1
		if scoreX==3:
			return infinity
		elif scoreO==3:
			return -infinity
		else:
			if scoreX>0:
				nX+=1
			if scoreO>0:
				nO+=1
	return nX-nO
'''
this Function below (named scoreWithDepth()) is the Core of this AI program here.
This function works on the concept of "NegMax" algortihm with the Added Optimization of Alpha-Beta Pruning
'''
plydepth=7
movesRemaining=9
def scoreWithDepth(Aimove,Playermove,depth,alpha,beta):
	maxScore=alpha#by default
	slot=-1#Null
	jslot=score()
	if Aimove=='O':
		jslot=-jslot
	global movesRemaining
	if jslot==infinity or jslot==-infinity or depth==0 or movesRemaining==0:
		return (slot,jslot)
	for islot in {0,1,2,3,4,5,6,7,8}:
		if board[islot]==' ':
			board[islot]=Aimove
			movesRemaining-=1
			(jslot,jmax)=scoreWithDepth(Playermove,Aimove,depth-1,-beta,-alpha)
			board[islot]=' '
			movesRemaining+=1
			jmax=-jmax
			if jmax>maxScore:
				maxScore=alpha=jmax
				slot=islot
		if alpha>=beta:#Pruning
			return(slot,maxScore)
	return (slot,maxScore)
#---Execution starts here
gameOver=False
i=nextMove
if random.randint(0,1)==1:#Player gets a random chance to play as O (first) or X(second)
	i=othermove
def press(btn):
	global gameOver
	if(gameOver==False):
		index=int(btn.__str__())
		if(board[index]==" "):
			global nextMove
			app.setButton(btn.__str__(),nextMove)
			board[int(btn.__str__())]=nextMove
			curscore=score()
			global othermove
			k=nextMove
			nextMove=othermove
			othermove=k
			global movesRemaining
			movesRemaining=movesRemaining-1
			if(curscore>=infinity):
				if(i=='X'):
					app.setLabel("print","Player Wins! ")
				else:
					app.setLabel("print","AI Wins! ")
				gameOver=True
			elif(curscore<=-infinity):
				if(i=='O'):
					app.setLabel("print","Player Wins! ")
				else:
					app.setLabel("print","AI Wins! ")
				gameOver=True
			elif movesRemaining==0:
				gameOver=True
				app.setLabel("print","It's a draw!")
			if(i!=nextMove and gameOver==False):
				MakeAiMove()
def reset(btn):
	for j in {0,1,2,3,4,5,6,7,8}:
		board[j]=" "
		app.setButton(j.__str__()," ")
	global gameOver
	gameOver=False
	global i
	i=nextMove
	if(random.randint(0,1)==0):
		i=othermove
	global movesRemaining
	movesRemaining=9
	app.setLabel("print","")
	if(i!=nextMove):
		MakeAiMove()
def MakeAiMove():
	(islot,iscore)=scoreWithDepth(nextMove,othermove,plydepth,(infinity*plydepth*(-1)),infinity*plydepth)#This is the core of the AI
	print("Score : "+iscore.__str__()+" , Box : "+islot.__str__())
	buttons[islot].invoke()
app = gui()
app.setSticky("news")
app.setExpand("both")
app.setFont(15)
buttons=[
app.addNamedButton(" ","0",press,0,0),
app.addNamedButton(" ","1",press,0,1),
app.addNamedButton(" ","2",press,0,2),
app.addNamedButton(" ","3",press,1,0),
app.addNamedButton(" ","4",press,1,1),
app.addNamedButton(" ","5",press,1,2),
app.addNamedButton(" ","6",press,2,0),
app.addNamedButton(" ","7",press,2,1),
app.addNamedButton(" ","8",press,2,2),
app.addButton("reset",reset,colspan=3),
app.addLabel("print","",colspan=3)
]
if(i!=nextMove):
	MakeAiMove()
app.go();