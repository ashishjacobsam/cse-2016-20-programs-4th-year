#Program to show List operations.
choice=1
List=[]
while choice!=5:
	print("1: Add a number to List")
	print("2: Remove a number from the List")
	print("3: Show the List")
	print("4: Search element")
	print("5: Exit Program")
	choice=int(input("Enter your choice : "))
	if(choice==1):
		k=int(input("Enter the number to insert in the list :"))
		List.append(k)
	elif choice==2:
		k=int(input("Enter the number to remove in the list :"))
		if(List.__contains__(k)):
			List.remove(k);
			print("Element removed successfully")
		else:
			print("Element not found")
	elif choice==3:
		print(List)
	elif choice==4:
		k=int(input("Enter the number to search in the list :"))
		if(List.__contains__(k)):
			ind=List.index(k)
			print("Element found at Index : "+ind.__str__() )
		else:
			print("Element not found in list")
	elif choice!=5:
		print("Invalid Input")
	print("*********************************************")