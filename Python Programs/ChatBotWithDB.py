#I am using MSSQL DB. If you want to use MySQL or anything else, 
#you will need to change the connectors and make sure that your said connector is installed and functioning
#To install any connector you need to use pip install from Command prompt. (There can be other ways too...)
import difflib;	#This is for checking difference between words
import pyodbc;	#This is the library for MSSQL. Change this if you need MySQL
class AiQues:
	def __init__(self, arg1, arg2):
		self.queslist=arg1
		self.response=arg2
	def DefaultCheck(self, parameter_list):
		if parameter_list in self.queslist:
			print("JC: "+self.response)
			return True
		return False
	def StrictCheck(self, parameter_list,slack):
		for x in self.queslist:
			diff=0
			for i,s in enumerate(difflib.ndiff(x, parameter_list)):# This function calculates the difference in two words
				if s[0]=='-':
					diff=diff+1
				elif s[0]=='+':
					diff=diff+1
			if diff < slack:		#If difference is less than the slack I'm giving, Accept it 
				print("JC: "+self.response)
				return True
		return False

server = '(localdb)\MSSQLLocalDB' 
database = 'ChatBot'  
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';')
#connection is database dependant. It will change if you are using MySQL

#The rest of the code remains the same irrespective of which DB service you are using
cursor = cnxn.cursor()

cursor.execute('SELECT phrase,response,phrase.respid FROM phrase,resp where phrase.respid=resp.respid')

EndQues=AiQues([],"")
endvals=cursor.execute("SELECT phrase,response FROM phrase,resp where phrase.respid=resp.respid AND phrase.respid=5").fetchall()
for row in endvals:
	EndQues.queslist.append(row[0])
	EndQues.response=row[1]


QuesList=[]
for id in {1,2,3,6,7,8}:
	endvals=cursor.execute("SELECT phrase,response FROM phrase,resp where phrase.respid=resp.respid AND phrase.respid="+id.__str__()).fetchall()
	obj=AiQues([],"")
	for row in endvals:
		obj.queslist.append(row[0])
		obj.response=row[1]
	QuesList.append(obj)

NullStr=cursor.execute("SELECT response FROM RESP WHERE RESPID=4").fetchone()[0];

print("JC: Hello. How may I help you?")

while 1:
	xx=input("User :").upper().split(" ")
	i=1
	cursor.execute('SELECT phrase,response,phrase.respid FROM phrase,resp where phrase.respid=resp.respid')
	for x in xx:
		
		if EndQues.DefaultCheck(x):
			exit()
		else:
			for y in QuesList:
				if y.DefaultCheck(x):#Check for exact word
					i=0
	if i==1:		
		for x in xx:
			if EndQues.StrictCheck(x,2):
				exit()
			else:
				for y in QuesList:
						if y.StrictCheck(x,2):#Check with some slack
							i=0
	if i==1:
		print("JC: "+NullStr)#Basically the bot says something like "I'm smart, but not so smart, okay?"

'''
This is the table I have created.

|--------|---------------------------------------------------------------------------------------------------------------------|
| Phrase | Response                                                                                                            |
|--------|---------------------------------------------------------------------------------------------------------------------|
| BYE    | Bye                                                                                                                 |
| CLOSE  | Bye                                                                                                                 |
| EXIT   | Bye                                                                                                                 |
| HELLO  | Greetings, Human.                                                                                                   |
| HEY    | Greetings, Human.                                                                                                   |
| HI     | Greetings, Human.                                                                                                   |
| IDE    | A great IDE to start with is Visual Studio Code. If you need specifically for python, PyCharm is also a good choice |
| LEARN  | You can head over to the official site of python. It is a great place to learn                                      |
| NAME   | I am labeled J.Chatbot.                                                                                             |
| PYTHON | To start coding in Python, Please download python along with an IDE of your choice                                  |
| SETUP  | To start coding in Python, Please download python along with an IDE of your choice                                  |
| TEACH  | You can head over to the official site of python. It is a great place to learn                                      |
| THANK  | No problem.                                                                                                         |
| WHO    | I am labeled J.Chatbot.                                                                                             |
|--------|---------------------------------------------------------------------------------------------------------------------|


Sample Output of program:

JC: Hello. How may I help you?
User :Hi
JC: Greetings, Human.
User :Who are you?
JC: I am labeled J.Chatbot.
User :I need help in python
JC: To start coding in Python, Please download python along with an IDE of your choice
User :Which IDE should I use?
JC: A great IDE to start with is Visual Studio Code. If you need specifically for python, PyCharm is also a good choice
User :How can i learn it?
JC: You can head over to the official site of python. It is a great place to learn
User :Okay, thanks
JC: No problem.
User :okay, bye
JC: Bye

'''
