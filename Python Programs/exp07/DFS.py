depth=int(input("Enter the depth of the complete binary tree : "))
vertices=(1<<(depth+1))-1
AdjMat=[[0 for i in range(vertices)] for j in range(vertices)]
for i in range((vertices//2)):
	j=(i*2)+1
	k=j+1
	AdjMat[j][i]=AdjMat[i][j]=AdjMat[i][k]=AdjMat[k][i]=1
def DepthFirstSearch(startVertex,target):
	Stack=[startVertex]
	Visited=[startVertex]
	while(Stack.__len__()>0):
		val=Stack.pop()
		print("Now at node : "+val.__str__())
		if(val==target):
			print("Target found!")
			return
		else:
			for j in range(vertices):
				if AdjMat[val][j]==1 and Visited.__contains__(j)==False:
					Stack.append(j)
					Visited.append(j)
	print("Target node not found")
search=int(input("Enter the vertex to search : "))
start=0
DepthFirstSearch(start,search)