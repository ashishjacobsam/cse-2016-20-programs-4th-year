#Program to show List operations.
choice=1
SetA=set()
SetB=set()
while choice!=9:
	print("1: Add a number to a SetA")
	print("2: Remove a number from a SetA")
	print("3: Add a number to a SetB")
	print("4: Remove a number from a SetB")
	print("5: Show contents of Sets")
	print("6: Search element")
	print("7: Get Union")
	print("8: Get Intersection")
	print("9: Exit")
	choice=int(input("Enter your choice : "))
	if(choice==1):
		k=int(input("Enter the number to insert in SetA :"))
		SetA.add(k)
	elif choice==2:
		k=int(input("Enter the number to remove from SetA :"))
		if(SetA.__contains__(k)):
			SetA.remove(k);
			print("Element removed successfully")
		else:
			print("Element not found")
	elif(choice==3):
		k=int(input("Enter the number to insert in SetB :"))
		SetB.add(k)
	elif choice==4:
		k=int(input("Enter the number to remove from SetB :"))
		if(SetB.__contains__(k)):
			SetB.remove(k);
			print("Element removed successfully")
		else:
			print("Element not found")
	elif choice==5:
		print("Set A :"+str(SetA))
		print("Set B :"+str(SetB))
	elif choice==6:
		k=int(input("Enter the number to search in the list :"))
		if(SetA.__contains__(k)):
			print("Element found at SetA")
		else:
			print("Element not found in SetA")
		if(SetB.__contains__(k)):
			print("Element found at SetB ")
		else:
			print("Element not found in SetB")
	elif choice==7:
		print(SetA.union(SetB))
	elif choice==8:
		print(SetA.intersection(SetB))
	elif choice!=9:
		print("Invalid Input")
	print("********************************************************")