//This is a different and (arguably) more dogmatic approach for making a DFA.
//If you are using the Stone-Aged Borland compiler and "bool" is undefined, just replace it with int.
//Actually you can do that with this Macro below. Uncomment it and it should work. 

//#define bool int //This will replace all ouccerance of bool with int

//Similarly, C++11 uses nullptr over NULL so you might need to uncomment the below macro as well

//#define nullptr NULL	//Now you can Nullify pointers in the depriciated compilers too. YAY!!!

//Class for state management
class State
{
	protected:		//In case you want to inherit this and add features, You might want to keep the stuff protected instead of private
	int id;			//Name of this state will be "Q"+id
	bool isFinal;
	State **Delta;	//An array of references (or simply, an array of State* pointers)
	char *Input;	//An array of accepted input characters
	int Length;		//Since this is a DFA, length of Delta[] = Length of Input[]
	public:
	State(int i,bool f,char* in,int l,State **de)
	{
		id=i;
		isFinal=f;
		Delta=de;
		Input=in;
		Length=l;
	}
	void SetDelta(State**d){Delta=d;}
	int ID(){return id;}
	bool IsFinal(){return isFinal;}
	State *Transition(char c)
	{
		for(int i=0;i<Length;i++)
		{
			if(c==Input[i])return Delta[i];
		}
		return nullptr;
	}
};

class DFA		//Formally, DFA is a quintouple. Let us keep it that way.
{
	State **AllState;// Array of states [Set Q]. Initial State[q0] is the first element of this array. Set of Final States[F] is a subarray of this array
	State *currentState;//This is the state object which will make transition as defined in it [delta]
	char *Inputs;	//All accepted input characters [Sigma].
	public:
	DFA(char *inputsArray,int inputsLength,int **TransitionTable,int States,int *FinalStatesIndex,int FinalLength)
	{//Now inputsArray is 1D array of length inputsLength, Transiion Table is a 2D matrix of inputsLength x StateLength
		AllState=new State*[States];
		int i,j;
		bool k;
		for(i=0;i<States;i++)
		{
			k=0;
			for(j=0;(j<FinalLength)&&!k;j++)
			{
				if(FinalStatesIndex[j]==i)
					k=1;
			}
			AllState[i]=new State(i,k,inputsArray,inputsLength,nullptr);
		}
		State**Del;
		for(i=0;i<States;i++)
		{
			Del=new State*[inputsLength];
			for(j=0;j<inputsLength;j++)
				Del[j]=AllState[TransitionTable[i][j]];
			AllState[i]->SetDelta(Del);
		}
		currentState=AllState[0];
	}
	int CurrnetState(){return currentState->ID();}
	bool Transition(char c)//Returns if the transition was successful or not
	{
		State *x=currentState->Transition(c);
		if(x==nullptr)return 0;
		currentState=x;
		return 1;
	}
	void Reset(){currentState=AllState[0];}//Going back to initial state
	bool isAccepted(){return currentState->IsFinal();}
};