#include<iostream>
#include "DFA.v2.cpp"	//This is taken from this .cpp file
using namespace std;

//#define bool int		//For all those using the Compiler from 1990s; Almost 3 Decades old as of now
/*
This is an example of how to implement your own DFA. 

Input the characters and transition table and the rest will be figured.

Accepts {"(a^n).(b^m)",where n%2==0 and m>1}
*/
int main()
{
	char xx[]={'a','b'},x;
	int **t=new int*[4],prev,curr;
	t[0]=new int[2]{1,2};//q0--a-> q1 || q0--b->q2
	t[1]=new int[2]{0,3};//q1--a-> q0 || q1--b->q3
	t[2]=new int[2]{3,2};//q2--a-> q3 || q2--b->q2
	t[3]=new int[2]{3,3};//q3--a-> q3 || q3--b->q3
	DFA dfa(xx,2,t,4,new int[1]{2},1);
	bool result;
	cout<<"Enter your input string: ";
	//That's all the initialization we need! Now to test it
	do
	{
		prev=dfa.CurrnetState();
		x=getchar();//One character at a time
		result=dfa.Transition(x);
		if(result)
		{
			cout<<"Previous State = Q"<<prev<<", Input = "<<x<<", Now at state = Q"<<dfa.CurrnetState();
			cout<<" || Accepted? :"<<(dfa.isAccepted()?"Yes":"No")<<"\n";
		}
		else
			cout<<"------------------------------------------------------------------------";
	}while(result);
	return 0;
}