#include <iostream>
#include "DFA.v2.cpp" //This is taken from this .cpp file
using namespace std;

//#define bool int		//For all those using the Compiler from 1990s; Almost 3 Decades old as of now
/*
This is an example of how to implement your own DFA. 

Input the characters and transition table and the rest will be figured.

Accepts All languages L such that third last element is a
*/
int main()
{
	char x;
	int prev;
	int *tt[8];
	tt[0]=new int[2]{1,0};//Q0--a-->Q1;Q0--b-->Q0
	tt[1]=new int[2]{2,3};//too lazy to write all that below
	tt[2]=new int[2]{4,5};
	tt[3]=new int[2]{6,7};
	tt[4]=new int[2]{4,5};
	tt[5]=new int[2]{6,7};
	tt[6]=new int[2]{2,3};
	tt[7]=new int[2]{1,0};//Transition table defined.
	DFA dfa(new char[2]{'a', 'b'}, 2,tt,8, new int[4]{4,5,6,7}, 4);
	bool result;
	cout << "Enter your input string: ";
	//That's all the initialization we need! Now to test it
	do
	{
		prev = dfa.CurrnetState();
		x = getchar(); //One character at a time
		result = dfa.Transition(x);
		if (result)
		{
			cout << "Previous State = Q" << prev << ", Input = " << x << ", Now at state = Q" << dfa.CurrnetState();
			cout << " || Accepted? :" << (dfa.isAccepted() ? "Yes" : "No") << "\n";
		}
		else
			cout << "------------------------------------------------------------------------";
	} while (result);
	return 0;
}