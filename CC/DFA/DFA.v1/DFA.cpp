class DFA	//DFA is a quintouple. So Implement them all and you're done!
{
	protected:
	char* Input;//Set of letters
	int InputLength;//Because in C/C++ it's handy to store the Length of an arrau
	int* Q;//Set (Array) of states
	int QLength;//Length of Q array
	int q0;//[Index] of initial state
	int *F;
	int FLength;
	int current;
	virtual int Delta(int stateIndex,int CharIndex)=0;
	public:
	int Current(){return current;}
	DFA(char *iS,int islen,int *iQ,int iqllen,int iq0,int *iF,int iflen)
	{
		Input=iS;
		InputLength=islen;
		Q=iQ;
		QLength=iqllen;
		q0=iq0;
		F=iF;
		FLength=iflen;
		current=q0;
	}
	void Reset(){current=q0;}
	int Accepted()
	{
		for(int i=0;i<FLength;i++){if(F[i]==current)return 1;}
		return 0;
	}
	int GetCharIndex(char c)
	{
		for(int i=0;i<InputLength;i++)
		{
			if(Input[i]==c)return i;
		}
		return -1;
	}
};
/*
This is an abstract Implementation of a DFA. 

Inherit it and add your customizations
*/