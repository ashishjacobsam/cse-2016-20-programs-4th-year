#include<iostream>
#include "DFA.cpp"	//This is taken from this .cpp file
using namespace std;
/*
This is an example of how to implement your own DFA. 

Technically, all you need to change is the Transition function(Delta)
Also, Specify how you want to print the output of it.

//Accepts {"(a^n).(b^m)",where n%2==0 and m>1}
*/
class CustomDFA:DFA
{
	public:
	CustomDFA():DFA(new char[2]{'a','b'},2,new int[5]{0,1,2,3,4},5,0,new int[1]{3},1){}
	int Delta(int i,int j)
	{
		switch (i)
		{
		case 0:
			if(j==0)return 1;
			else return 3;
			//I don't need break statement becuase return exits from here
		case 1:
			if(j==0)return 2;
			else return 4;
		case 2:
			if(j==0)return 1;
			else return 3;
		case 3:
			if(j==0)return 4;
			else return 3;
		default:return 4;
		}
	}
	int Inputs(char c)
	{
		int ic=GetCharIndex(c);
		if(ic==-1)
		cout<<"-------------------------";
		else
		{
			cout<<"With Input "<<Input[ic]<<", Old state = Q"<<current;
			current=Delta(current,ic);
			cout<<" ;Current state =Q"<<current<<"\n Accepted : "<<(Accepted()?"True":"False")<<"\n";
			return 1;
		}
		return 0;
	}
};
int main()
{
	CustomDFA cdfa;
	cout<<"Enter a string: ";
	int result;
	do
	{
		char c=getchar();
		result=cdfa.Inputs(c);
	}while(result);
	return 0;
}