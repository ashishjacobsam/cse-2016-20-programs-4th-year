#include<iostream>
#include "DFA.cpp"
using namespace std;
//Accepts all words ending with "aba"
class CustomDFA:DFA
{
	public:
	CustomDFA():DFA(new char[2]{'a','b'},2,new int[4]{0,1,2,3},4,0,new int[1]{3},1){}
	int Delta(int i,int j)
	{
		switch (i)
		{
		case 0:
			if(j==0)return 1;
			else return 0;
			//I don't need break statement becuase return exits from here
		case 1:
			if(j==0)return 1;
			else return 2;
		case 2:
			if(j==0)return 3;
			else return 0;
		case 3:
			if(j==0)return 1;
			else return 2;
		}
		return 0;
	}
	int Inputs(char c)
	{
		int ic=GetCharIndex(c);
		if(ic==-1)
		cout<<"-------------------------";
		else
		{
			cout<<"With Input "<<Input[ic]<<", Old state = Q"<<current;
			current=Delta(current,ic);
			cout<<" ;Current state =Q"<<current<<"\n Accepted : "<<(Accepted()?"True":"False")<<"\n";
			return 1;
		}
		return 0;
	}
};
int main()
{
	CustomDFA cdfa;
	cout<<"Enter a string: ";
	int result;
	do
	{
		char c=getchar();
		result=cdfa.Inputs(c);
	}while(result);
	return 0;
}