#include<iostream>
#include "DFA.cpp"
using namespace std;
//Accepts all words with last element as 'a'
class CustomDFA:DFA
{
	public:
	CustomDFA():DFA(new char[2]{'a','b'},2,new int[2]{0,1},2,0,new int[1]{1},1){}
	int Delta(int i,int j)
	{
		switch (i)
		{
		case 0:			//for state Q0
			if(j==0)return 1;
			else return 0;
						//I don't need break statement becuase return exits from here
		default:		//for state Q1
			if(j==0)return 1;
			else return 0;
		}
	}
	int Inputs(char c)
	{
		int ic=GetCharIndex(c);
		if(ic==-1)
		cout<<"-------------------------";
		else
		{
			cout<<"With Input "<<Input[ic]<<", Old state = Q"<<current;
			current=Delta(current,ic);
			cout<<" ;Current state =Q"<<current<<"\n Accepted : "<<(Accepted()?"True":"False")<<"\n";
			return 1;
		}
		return 0;
	}
};
int main()
{
	CustomDFA cdfa;
	cout<<"Enter a string: ";
	int result;
	do
	{
		char c=getchar();
		result=cdfa.Inputs(c);
	}while(result);
	return 0;
}