//The most minimalistic DFA yet. The inputting of Transition table is a bit less troublesome, but more prone to failure if not done correctly
class DFA
{
	int CurrentState,StateCount,InputSymbolCount,*FinalState,FinalStateCount,**Production;
	char *InputSymbol;
	int GetInputSymbolIndex(char c)
	{
		for(int i=0;i<InputSymbolCount;i++){if(InputSymbol[i]==c)return i;}
		return -1;
	}
public:
	DFA(int stateCount,int inputSymbolCount,char*inputSymbol,int finalStateCount,int *finalState,int* X)
	{
		StateCount=stateCount;
		InputSymbolCount=inputSymbolCount;
		FinalStateCount=finalStateCount;
		FinalState=finalState;
		InputSymbol=inputSymbol;
		CurrentState=0;
		Production=new int*[StateCount];
		for(int i=0;i<StateCount;i++)
		{
			Production[i]=new int[InputSymbolCount];
			for(int j=0;j<InputSymbolCount;j++,X++)
				Production[i][j]=*X;
		}
	}
	int isFinal()
	{
		for(int i=0;i<FinalStateCount;i++)if(FinalState[i]==CurrentState)return 1;
		else return 0;
	}
	int ShowCurrnetState(){return CurrentState;}
	int Input(char c)//Returns if the input was accepted successfully or not
	{
		int result=GetInputSymbolIndex(c);
		if(result==-1)return 0;//Input was not accepted
		else CurrentState=Production[CurrentState][result];
		return 1;//Input was accepted
	}
};