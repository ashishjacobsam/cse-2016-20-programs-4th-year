#include<iostream>
#include "DFA.v3.cpp"	//This is taken from this .cpp file
using namespace std;

//#define bool int		//For all those using the Compiler from 1990s; Almost 3 Decades old as of now
/*
This is an example of how to implement your own DFA. 

Input the characters and transition table and the rest will be figured.

Accepts {"(a^n).(b^m)",where n%2==0 and m>1}
*/
int main()
{
	char x;
	int prev;
	DFA dfa(4,2,new char[2]{'a','b'},1,new int[1]{2},new int[8]{1,2,0,3,3,2,3,3});//All transition table at one Go
	//Q0('a')=Q1; Q0('b') = 2 , ... so on
	bool result;
	cout<<"Enter your input string: ";
	//That's all the initialization we need! Now to test it
	do
	{
		prev=dfa.ShowCurrnetState();
		x=getchar();//One character at a time
		result=dfa.Input(x);
		if(result)
		{
			cout<<"Previous State = Q"<<prev<<", Input = "<<x<<", Now at state = Q"<<dfa.ShowCurrnetState();
			cout<<" || Accepted? :"<<(dfa.isFinal()?"Yes":"No")<<"\n";
		}
		else
			cout<<"------------------------------------------------------------------------";
	}while(result);
	return 0;
}