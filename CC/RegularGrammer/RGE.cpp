#include "Tree.cpp"
#include "..\..\Custom-DSA\BitArray.cpp"
char epsilon = '#';
int strlength(const char *str) //Because I'm not including an entire header just for a small function
{
	const char *s;
	for (s = str; *s; ++s); //Ninja technique for traversing a string
	return (s - str);
}
//This is a Top Down Recusive Decent Backtracking Parser.
//This means it does not support Left Recursive grammer, Unambigious grammer and non-deterministic grammer
class Grammer
{
	char *Tokens;
	int VariableLength, TerminalLength;
	int ***Transitions;
	int *TransitionCount;
	int **TransitionLength;
	int GetSymbolIndex(char c)
	{
		if(c==epsilon)return VariableLength+TerminalLength;
		for (int i = 0; i < (VariableLength + TerminalLength); i++)
			if (c == Tokens[i])
				return i;
		return -1;
	}
	int LeftParse(char string[], int size, int &complete, int Symbol, TreeNode *stack)
	{
		if ((complete > size) || (Symbol >= VariableLength + TerminalLength) || (Symbol < 0))
			return 0;
		else if (Symbol >= VariableLength) //Token is a Terminal
		{
			if (Tokens[Symbol] == string[complete])
			{
				complete++; //We have moved ahead!
				return 1;   //Partially complete
			}
			else
				return 0; //Trivial Reject
		}
		else
		{
			for (int i = 0; i < TransitionCount[Symbol]; i++) //Process all Transitions related with this Variable
			{
				int Length = TransitionLength[Symbol][i], j = 0, res; //Get the Length of the ith Transition of the Variable
				if (Length > 0)
				{
					stack->SetChildren(Length); //We are gonna test this transtion out, so setting it up
					while ((j < Length))
					{
						TreeNode *jChild = new TreeNode(Tokens[Transitions[Symbol][i][j]]);
						res = LeftParse(string, size, complete, Transitions[Symbol][i][j], jChild);
						if (res == 0) //Recursivly check this symbol
						{
							delete jChild; //KILL THE CHILD!!!
							goto ex;	   //Since it is rejected, exit from loop
						}
						else							//This is accepted. Now build the Parse Tree
							stack->Child[j++] = jChild; //Attach the child to its root
					}
				}
				else
				{
					stack->SetChildren(1);				 //The Tree should expand to 1 child - Epsilon
					stack->Child[0] = new TreeNode(' '); //Insert Symbl of epsilon
				}
				if (complete < size)
					return 1; //More symbols remain
				else
					return 2; //This is completely parsed
			ex:;
			}
		}
		return 0; //We have exhauster all options!
	}

public:
	Grammer(int v, int t, char tok[]) : Tokens(tok)
	{
		VariableLength = v;
		TerminalLength = t;
		Transitions = new int **[VariableLength];
	}
	Grammer(int v, int t, char *tok, int tc[], char ***T) : Tokens(tok), TransitionCount(tc) //OneShot definition
	{
		VariableLength = v;
		TerminalLength = t;
		Transitions = new int **[VariableLength];
		TransitionLength = new int *[VariableLength];
		for (int i = 0; i < VariableLength; i++)
		{
			Transitions[i] = new int *[TransitionCount[i]];
			TransitionLength[i] = new int[TransitionCount[i]];
			for (int j = 0; j < TransitionCount[i]; j++)
			{
				int size = strlength(T[i][j]);
				TransitionLength[i][j] = size;
				Transitions[i][j] = new int[size];
				for (int k = 0; k < size; k++)
				{
					int index = GetSymbolIndex(T[i][j][k]);
					Transitions[i][j][k] = index;
				}
			}
		}
	}
	int GetIndex(char a) //Gets the index of the character a in the Input Set if it exists, otherwise returns null
	{
		for (int i = VariableLength; i < VariableLength + TerminalLength; i++)
		{
			if (Tokens[i] == a)
				return i;
		}
		return -1;
	}
	TreeNode *LeftParse(char string[]) //The public function which does the parsing
	{
		TreeNode *tree = new TreeNode(Tokens[0]);
		int size = strlength(string);
		int complete = 0;
		return LeftParse(string, size, complete, 0, tree) > 1 ? tree : nullptr;
	}
	void FirstFollow(char **&First, char **&Follow);
};