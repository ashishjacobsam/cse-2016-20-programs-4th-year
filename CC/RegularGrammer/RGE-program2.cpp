#include<iostream>
#include "RGE.cpp"
//#define nullptr NULL	//For legacy compatiblity
using namespace std;
int main()
{
	Grammer g
	(
		5,//5 Variables E,A,T,B,F
		32,//{26 alphabets, ),(,*,+,-,/}= 32 terminals
		"EATBF()+-*/abcdefghijklmnopqrstuvwxyz",//All the Variables (first) and then all the Terminals
		new int[5]{1,3,1,3,27},//0th Variable(E) has 1 Transition, 1st Variable(A) has 3 transitions, and so on...
		new char**[5]//The respective transitions in the order given
		{
			new char*[1]{"TA"},//The Transitions of E
			new char*[3]{"+TA","-TA",""},//Transitions of A
			new char*[1]{"FB"},//The Transitions of T
			new char*[3]{"*FB","/FB",""},//Transtitions of B
			new char*[27]{"(E)","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"},//Transitions of F
		}
	);
	char input[100];
	cout<<"Enter input for grammer :";
	cin>>input;
	TreeNode*res=g.LeftParse(input);
	if(res)
	{
		cout<<"Accepted. Printing Tree :\n";
		TreePrint(res);
	}
	else cout<<"Rejected\n";
	return 0;
}