#include<iostream>
using namespace std;
class TreeNode
{
	public: 
	char Value;
	int size;
	TreeNode **Child;//1D array of pointers
	TreeNode(){Value=' ';Child=nullptr;}//By default
	TreeNode(char Val)
	{
		Value=Val;
		Child=nullptr;
		size=0;
	}
	void SetChildren(int i)//SetChildren has to be called before Setting the value of Child array
	{
		size=i;
		Child=new TreeNode*[i];
	}
	~TreeNode(){if(Child!=nullptr)delete[] Child;}//You need to kill the children before the parent (Don't take this out of context, alright?)
};
void TreePrint(TreeNode *tree,int depth=0,int first=1)
{
	if(first==0){for(int i=0;i<depth;i++)cout<<"   |";}
	cout<<"-->";
	cout<<tree->Value;
	if(tree->size==0)cout<<"\n";
	else
	{
		int len=(tree->size)-1;
		TreePrint(tree->Child[0],depth+1,1);
		for(int i=1;i<=len;i++)TreePrint(tree->Child[i],depth+1,0);
	}
}