#include<iostream>
#include "RGE.cpp"
int main()
{
	Grammer g
	(
		2,//2 Variables S and A
		2,//2 Termainals a and b
		"SAab",//All the Variables (first) and then all the Terminals
		new int[2]{2,2},//0th Variable(S) has 2 Transitions, 1st Variable(A) has 2 transitions
		new char**[2]//The respective transitions in the order given
		{
			new char*[2]{"aA","b"},//The Transitions of S
			new char*[2]{"bS","a"}//The Transitions of A
		}
	);
	char input[100];
	cout<<"Enter input for grammer :";
	cin>>input;
	TreeNode*res=g.LeftParse(input);
	if(res)
	{
		cout<<"Accepted.";
	}
	else cout<<"Rejected.";
	return 0;
}