#include<stdio.h>//Now this should be compatible for even your BSoD-esque text-editor featuring compiler
#define OpCon(input) input=='+'||input=='-'||input=='*'||input=='/'||input=='('||input==')'||input==';'||input=='='||input==','
#define sn(input) input==' '||input=='\n'||input=='\t'
#define Alpha(input) (input>='A'&&input<='Z')||(input>='a'&&input<='z')
#define Num(input) input>='0'&&input<='9'
#define Valid(input) OpCon(input)||sn(input)||Alpha(input)||Num(input)
int main()
{
	printf("Enter your input here :\n");//You can use the input redirection operator for file input
	char input=getchar();
	int state=0,pre;
	int TokenCount=0;
	do
	{
		pre=state;
		switch (state)//If you look closely, you can deduce that this is an implementation of a DFA
		{
		case 0:
			if(Alpha(input))state=1;
			else if(Num(input))state=2;
			else if(OpCon(input))state=3;
			break;
		case 1:
			if(OpCon(input))state=3;
			else if(sn(input))state=0;
			break;
		case 2:
			if(sn(input))state=0;
			else if(Alpha(input))state=1;
			else if(OpCon(input))state=3;
			break;
		case 3:
			if(sn(input))state=0;
			else if(Alpha(input))state=1;
			else if(Num(input))state=2;
		}
		if(state!=pre&&state!=0)
		{
			printf("|");
			TokenCount++;
		}
		if(state!=0) printf("%c",input);//If you look here, you might correct yourself in saying this is more of a mealy machine
		input=getchar();//Take the next input. If you're not using input redirection, but are on the console use a character like '$' , '@' or anything that might make valid(input) macro false
	} while (Valid(input));//See, this is the only reason why I created all those #defines in the first place!
	printf("|\nNo. of Tokens = %d",TokenCount);
	return 0;
}