The program is suppossed to do is to read the contents of a file and "tokenize" it. The program on the surface seems to be able handle input only on the console. There is no File Handling here. How to add the file handling functionality without changing any code? Two words : Input Redirection

The standard input buffer is designed for input redirection. Suppose we have input stored in a file, say "input.txt" and the program which is to be fed the input is named "z.exe"
Then, Input Redirection will be done by the command :

z.exe < input.txt

This is supported in cmd as well as ubuntu (But for some reason is disabled in Powershell)
The program "z.exe" is executed as it would have for the inputs of "input.txt"
You can have some more fun with the output redirection as well. Typically, you can have the output of a program dumped in a file as follows:

z.exe > output.txt

This commands creates (or overwrites for already existing) file "output.txt" containing whatever output the program generates.
To use both Input and output generation, you can use the command

z.exe < input.txt > output.txt

This feeds the contents of the file "input.txt" to the program "z.exe" and dumps its output to a file named "output.txt" which would be created/overwritten