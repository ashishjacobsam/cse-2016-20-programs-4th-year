#define isAlpha(x) x>='a'&&x<='z'
typedef unsigned int uint;
char epsilon='#';
class Vertex;//Forward Declaration
struct Edge
{
public:
	char Symbol;
	Vertex *Origin, *Dest;
	Edge *Next;//part of Unidirectional Stack
	Edge(char c,Vertex *f,Vertex *t)
	{
		Symbol=c;Origin=f;Dest=t;Next=nullptr;
	}
};
struct EdgeList
{
	Edge *Head;
	EdgeList(){Head=nullptr;}
	void AddEdge(Edge *e)
	{
		e->Next=Head;
		Head=e;
	}
};
class Vertex
{
public:
	uint id;
	int isFinal;
	EdgeList Incoming,Outgoing;
	Vertex(uint x,int final=0){id=x;isFinal=final;}
	void AddEdge(Vertex *v,char c)
	{
		Edge *e1=new Edge(c,this,v),*e2=new Edge(c,this,v);
		Outgoing.AddEdge(e1);
		v->Incoming.AddEdge(e2);
	}//Problably won't need to remove edge
};
int isValidChar(char c)
{
	if((c>='a'&&c<='z')||c=='|'||c=='*'||c==')'||c=='('||c=='+')return 1;
	return 0;
}