#include<iostream>
#include "../../Custom-DSA/BitArray.cpp"
#include "r2f.cpp"
using namespace std;

struct StackNode
{
	char op;
	Vertex *Head,*Tail;
	StackNode *Next;
};

struct QueueNode
{
	Vertex *V;
	QueueNode *Next,*Prev;
	QueueNode(Vertex* v)
	{
		V=v;
		Next=Prev=nullptr;
	}
	~QueueNode()
	{
		V=nullptr;
		Next=Prev=nullptr;
	}
};
uint counter;
void Print(Vertex *x)
{
	BitArray array(counter);
	array.Set(x->id);
	QueueNode *stack=new QueueNode(x);
	QueueNode *tail=stack,*delthis;
	Edge *ie;
	do
	{
		cout<<"\nIn Vertex no. "<<stack->V->id;
		for(ie=stack->V->Outgoing.Head;ie!=nullptr;ie=ie->Next)
		{
			cout<<"\nEdge "<<ie->Origin->id<<" ---"<<ie->Symbol<<"-->"<<ie->Dest->id;
			if(array.Get(ie->Dest->id)==0)
			{
				if(stack==nullptr)stack=tail=new QueueNode(ie->Dest);
				else tail=tail->Next=new QueueNode(ie->Dest);
				array.Set(ie->Dest->id);
			}
		}
		cout<<"\n---------------------\n";
		delthis=stack;
		stack=stack->Next;
		delete delthis;
		delthis=nullptr;
	}while(stack!=nullptr);
	cout<<"==============================================\n";
}
Vertex* Create(){return new Vertex(counter++);}
StackNode *Stack=nullptr;
void AddtoStack(char InputOperator)
{
	StackNode *node=new StackNode;
	node->op=InputOperator;
	node->Tail=node->Head=Create();
	node->Next=Stack;
	Stack=node;
}
char PopStack()
{
	if(Stack==nullptr)return 0;
	StackNode*delthis=Stack;
	Stack=Stack->Next;
	char c=delthis->op;
	Vertex *h=delthis->Head,*t=delthis->Tail;
	delthis->Head=delthis->Tail=nullptr;
	if(c=='|')//It can be either '(', or '$'
	{
		Stack->Head->AddEdge(h,epsilon);
		t->AddEdge(Stack->Tail,epsilon);
	}
	else 
	{
		Stack->Tail->AddEdge(h,epsilon);
		Stack->Tail=t;
	}//Don't delete the nodes because we want to use them
	delete delthis;
	return c;
}
int main()
{
	counter=0;
	cout<<"Enter regular grammar : ";
	AddtoStack('$');
	char x=getchar();
	int y;
	while(isValidChar(x))
	{
		if(isAlpha(x))
		{
			Vertex *newvertex=Create();
			Stack->Tail->AddEdge(newvertex,x);
			Stack->Tail=newvertex;
		}
		else if(x=='|')
		{
			if(Stack->op=='|')y=PopStack();
			if(y=='$')
			{
				cout<<"Invalid Grammar! cannot form a NFA!";
				return 0;
			}
			AddtoStack(x);
		}
		else if(x=='*')
		{
			if(Stack->Head!=Stack->Tail)
			{
				Stack->Head->AddEdge(Stack->Tail,epsilon);
				Stack->Tail->AddEdge(Stack->Head,epsilon);
			}
			else
			{
				cout<<"Invalid Grammar! cannot form a NFA!";
				return 0;
			}
		}
		else if(x=='+')
		{
			if(Stack->Head!=Stack->Tail)Stack->Tail->AddEdge(Stack->Head,epsilon);
			else
			{
				cout<<"Invalid Grammar! cannot form a NFA!";
				return 0;
			}
		}
		else if(x==')') 
		{
			do
			{y=PopStack();}
			while(y!='('&&y!='$');
			if(y=='$')
			{
				cout<<"Invalid Grammar! cannot form a NFA!";
				return 0;
			}
		}
		else if(x=='(')	AddtoStack(x);
		x=getchar();
	}
	while(Stack->op!='$')PopStack();
	Stack->Tail->isFinal=1;
	cout<<"NFA Prepared. Enter the count of test cases : ";
	int cases;
	cin>>cases;

	QueueNode *CurrentNodes,*nd,*nextNodes,*inode;
	Edge *ie;
	BitArray done(counter);
	while(cases-->0)
	{
		CurrentNodes = new QueueNode(Stack->Head);
		done.ResetAll();
		done.Set(0);
		nextNodes=CurrentNodes;
		for(nd=CurrentNodes;nd!=nullptr;nd=nd->Next)
		{
			for(ie=nd->V->Outgoing.Head;ie!=nullptr;ie=ie->Next)
			{
				if(ie->Symbol==epsilon&&done.Get(ie->Dest->id)==0)
				{
					inode=new QueueNode(ie->Dest);
					nextNodes=nextNodes->Next=inode;
					done.Set(inode->V->id);
				}
			}
		}//CurrentNodes is now fully processed Merge
		cout<<"Enter Input :";
		do{x=getchar();}while(!(x>='a'&&x<='b'));
		while(x>='a'&&x<='z'&&CurrentNodes!=nullptr)
		{
			cout<<"------------------------------------------\n";
			cout<<"Initial State = { "<<CurrentNodes->V->id;
			for(nd=CurrentNodes->Next;nd!=nullptr;nd=nd->Next)cout<<", "<<nd->V->id;
			cout<<" }\nInput = "<<x<<"\nCurrent State = { ";

			nextNodes=nullptr;
			done.ResetAll();
			for(nd=CurrentNodes;nd!=nullptr;nd=CurrentNodes)
			{
				for(ie=nd->V->Outgoing.Head;ie!=nullptr;ie=ie->Next)
				{
					if(ie->Symbol==x&&done.Get(ie->Dest->id)==0)
					{
						nd=new QueueNode(ie->Dest);
						nd->Next=nextNodes;
						nextNodes=nd;
						done.Set(ie->Dest->id);
					}
				}
				nd=CurrentNodes;
				CurrentNodes=CurrentNodes->Next;
				delete nd;
				nd=CurrentNodes;
			}
			CurrentNodes=nextNodes;
			if(nextNodes!=nullptr)
			{
				while(nextNodes->Next!=nullptr)nextNodes=nextNodes->Next;
			}
			for(nd=CurrentNodes;nd!=nullptr;nd=nd->Next)
			{
				for(ie=nd->V->Outgoing.Head;ie!=nullptr;ie=ie->Next)
				{
					if(ie->Symbol==epsilon&&done.Get(ie->Dest->id)==0)
					{
						inode=new QueueNode(ie->Dest);
						nextNodes=nextNodes->Next=inode;
						done.Set(inode->V->id);
					}
				}
			}//CurrentNodes is now fully processed
			y=0;

			if(CurrentNodes==nullptr)
			{
				cout<<" NULL }\nFinal State : No";
				cout<<"\nThis string cannot be processed furthur\n";
				while(x>='a'&&x<='z')x=getchar();
			}
			else
			{
				cout<<CurrentNodes->V->id;
				for(nd=CurrentNodes->Next;nd!=nullptr;nd=nd->Next)cout<<", "<<nd->V->id;
				for(nd=CurrentNodes;nd!=nullptr;nd=nd->Next)if(nd->V->isFinal)y=1;
				cout<<" }\n";
				cout<<"Final State : "<<(y?"Yes\n":"No\n");
				x=getchar();
			}
		}
		cout<<"************************************************************\n";		
	}
	return 0;
}