#include <string.h>
#define Size 100
char ops[] = {'+', '-', '/', '*', '(', ')'};
int GetOpIndex(char c)
{
	for(int i=0;i<6;i++)
		if(ops[i]==c)return i;//Found at index i
	return -1;//Not Found
}
const char* InfixToPostfix(char* Input,int InpSize)
{
	char*Output=malloc((InpSize+1)*sizeof(char));//Dynamic Array Initialization of the 70s
	char OpStack[Size];
	int StackTop=-1;
	int iread=0,oread=0;
	do
	{
		char x = Input[iread++];
		if (x >= 'a' && x <= 'z')
			Output[oread++]=x; //x is an operand
		else		   //x must be an operator
		{
			int j = GetOpIndex(x);
			if (j != -1)
			{
				if (x == '(')
					OpStack[++StackTop]=x;
				else if (x == ')')
				{
					while (OpStack[StackTop] != '(')
						Output[oread++]=OpStack[StackTop--]; // Pop Everything until (
					StackTop--;//Pop the last (
				}
				else
				{
					while((StackTop>=0)&&GetOpIndex(OpStack[StackTop])/2<=j/2)//While lower precedence operator
						Output[oread++]=OpStack[StackTop--];//This is my ninja technique to manage Precedence as well check the operator
					OpStack[++StackTop]=x;
				}
			}
		}
	} while (iread<InpSize);
	while (StackTop>=0)//Empty the Stack
		Output[oread++]=OpStack[StackTop--];
	Output[oread]='\0';	//The Old-School sanitization of a string
	return Output;
}