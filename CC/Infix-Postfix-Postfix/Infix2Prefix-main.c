#include <stdio.h>
#include "Infix2Postfix.c"
int main()
{
	char c[100],*rev,*d;
	printf("Enter Infix expression : ");
	scanf("%s",c);
	int size=strlen(c);
	rev=strrev(c);
	for(int x=0;x<size;x++)
	{
		if(rev[x]==')')rev[x]='(';
		else if(rev[x]=='(')rev[x]=')';
	}
	d=strrev(InfixToPostfix(rev,size));
	printf("The Converted Prefix is : %s",d);
	return 0;
}