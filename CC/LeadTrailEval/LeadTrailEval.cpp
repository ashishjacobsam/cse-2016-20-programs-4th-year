#include <iostream>
#include <vector>
#include <string>
#include<algorithm>
using namespace std;
struct Grammar
{
	vector<char> Variable;
	vector<vector<string>> Productions;
	bool IsVariable(char c) { return c >= 'A' && c <= 'Z'; }
	int TerminalIndex(char c)
	{
		int i = 0;
		for (auto k : Variable)
		{
			if (k == c)return i;
			i++;
		}
		return -1;
	}
	vector<char> Leading(int index)
	{
		vector<char> Lead;
		for (auto p : Productions[index])
		{
			if (IsVariable(p[0]))
			{
				if (p.length() > 1)Lead.push_back(p[1]); //This is Operator grammer. the other one HAS to ber terminal if this grammer is valid
				auto pin = TerminalIndex(p[0]);
				if (index != pin)
				{
					auto y = Leading(pin);
					Lead.insert(Lead.end(), y.begin(), y.end());
				}
			}
			else Lead.push_back(p[0]);
		}
		return ReturnUnique(Lead);
	}
	vector<char> Trailing(int index)
	{
		vector<char> Trail;
		for (auto p : Productions[index])
		{
			int l = p.length() - 1;
			if (IsVariable(p[l]))
			{
				if (l > 0)Trail.push_back(p[l - 1]); //This is Operator grammer. the other one HAS to ber terminal if this grammer is valid
				auto pin=TerminalIndex(p[l]);
				if (index != pin)
				{
					auto y = Trailing(pin);
					Trail.insert(Trail.end(), y.begin(), y.end());
				}
			}
			else Trail.push_back(p[l]);
		}
		return ReturnUnique(Trail);
	}
	vector<char> ReturnUnique(vector<char> t)
	{
		sort(t.begin(),t.end());
		auto last=unique(t.begin(),t.end());
		t.erase(last,t.end());
		return t;
	}
};
int main()
{
	cout << "Enter no. of Variables :";
	Grammar g;
	int i, j, k, l, m, n;
	char c;
	cin >> l;
	cout << "Enter the " << l << " Variables : ";
	g.Productions = vector<vector<string>>(l);
	for (i = 0; i < l; i++)
	{
		cin >> c;
		g.Variable.push_back(c);
	}
	cout << "\nEnter Productions Saperated by |. Please do not use spaces\n";
	string input;
	for (i = 0; i < l; i++)
	{
		cout << g.Variable[i] << " --> ";
		cin >> input;
		k = input.length();
		m = n = 0;
		for (j = 0; j < k; j++, n++)
		{
			if (input[j] == '|')
			{
				auto zz = input.substr(m, n);
				g.Productions[i].push_back(zz);
				m = j + 1;
				n = 0;
			}
		}
		g.Productions[i].push_back(input.substr(m));
	}
	for (i = 0; i < l; i++)
	{
		cout << "For " << g.Variable[i] << "\nLeading : ";
		for (auto k : g.Leading(i))cout << k << " ";
		cout << "\nTrailing :  ";
		for (auto k : g.Trailing(i))cout << k << " ";
		cout << "\n";
	}
	return 0;
}