%{
#include<stdio.h>
#include<string.h>
%}

%%
$[a-z][+-X/][a-z](([+-X/][a-z])*)\n	{printf("Valid Token\n");}

\n	{printf("\n-------INVALID------------\n");}

%%

int yywrap(void){}
int main()
{
	yylex();
	return 0;
}