#include <iostream>
#include <vector>
using namespace std;
struct Grammar
{
	vector<char> V;					//Set of Variables, V[0] the start variable
	vector<char> T;					//Set of Terminals
	vector<vector<vector<char>>> P; //P[i] are the production rules of ith variable
	int GetVariableIndex(char c)
	{
		for (int i = 0; i < V.size(); i++)
		{
			if (V[i] == c)
				return i;
		}
		return -1;
	}
	int GetTerminalIndex(char c)
	{
		for (int i = 0; i < T.size(); i++)
		{
			if (T[i] == c)
				return i;
		}
		return -1;
	}
	int GetCharType(char c)
	{
		if (GetTerminalIndex(c) >= 0)
			return 1;
		else if (GetVariableIndex(c)>=0)
			return 2;
		else return 0;
	} //Terminal=1,Variable=2,None=0;
};
struct CannonicalProduction
{
	char LHS;
	int DotPos;
	int ProdIndex;
	CannonicalProduction()
	{
		LHS = DotPos = ProdIndex = -1;
	}
	vector<char> RHS(Grammar g) { return (g.P[g.GetVariableIndex(LHS)][ProdIndex]); }
	CannonicalProduction(char lhs = -1, int dp = -1, int pi = -1)
	{
		LHS = lhs;
		DotPos = dp;
		ProdIndex = pi;
	}
	bool isReduced(Grammar g) 
	{ 
		auto sz=RHS(g).size();
		return DotPos >= sz; 
	}
};
struct StateNode
{
	int I;
	vector<CannonicalProduction> ProdList;
	StateNode **OutgoingTerminals;
	StateNode **OutgoingVariables;
	StateNode(int x, int nT, int nV)
	{
		I = x;
		ProdList = vector<CannonicalProduction>();
		OutgoingTerminals = new StateNode*[nT]();
		OutgoingVariables = new StateNode*[nV]();
	}
	bool Exists(CannonicalProduction p)
	{
		for (auto x : ProdList)
		{
			if ((x.DotPos == p.DotPos) && (x.LHS == p.LHS) && (x.ProdIndex == p.ProdIndex))
				return true;
		}
		return false;
	}
};
struct LR0Parser
{
	Grammar AugmentedGrammar;
	int Accepted;
	vector<StateNode *> States;
	StateNode *Exists(StateNode *x)
	{
		for (auto p : States)
		{
			for (auto q : x->ProdList)
			{
				if (p->Exists(q) == false)
					goto nx;
				return p;
			}
		nx:;
		}
		return nullptr;
	}
	LR0Parser(Grammar g)
	{
		register int i,j,k;
		Accepted = -1;
		AugmentedGrammar=g;
		AugmentedGrammar.V.push_back('?');
		AugmentedGrammar.T.push_back('$');
		AugmentedGrammar.P.push_back(vector<vector<char>>(1));
		AugmentedGrammar.P[AugmentedGrammar.P.size()-1][0]={'S','$'};
		//Augmented Grammar Ready!!!
		int StateCount = -1;
		States = vector<StateNode *>();
		States.push_back(new StateNode(++StateCount, AugmentedGrammar.T.size(), AugmentedGrammar.V.size()));
		States[0]->ProdList.push_back(CannonicalProduction('?', 0, 0));
		for (j=0;j<States.size();j++)
		{
			auto x=States[j];
			vector<StateNode *> tt(AugmentedGrammar.T.size()), vt(AugmentedGrammar.V.size());
			for (k=0;k<x->ProdList.size();k++)
			{
				auto y=x->ProdList[k];
				int ps = AugmentedGrammar.P[AugmentedGrammar.GetVariableIndex(y.LHS)][y.ProdIndex].size();
				if (y.DotPos < ps) //DotPos is at a Symbol. Either Shift or Goto is to be performed now. Else, we know this is reduced
				{
					char t = AugmentedGrammar.P[AugmentedGrammar.GetVariableIndex(y.LHS)][y.ProdIndex][y.DotPos];
					if (AugmentedGrammar.GetCharType(t) == 2) //This is a Variable
					{
						int id = AugmentedGrammar.GetVariableIndex(t);
						for (ps = 0; ps < AugmentedGrammar.P[id].size(); ps++)
						{
							CannonicalProduction prod = CannonicalProduction(t, 0, ps);
							if (x->Exists(prod) == false)
								x->ProdList.push_back(prod); //Insert the Variable Production rules
						}
						if (vt[id] == nullptr)
							vt[id] = new StateNode(-1, AugmentedGrammar.T.size(), AugmentedGrammar.V.size());
						vt[id]->ProdList.push_back(CannonicalProduction(y.LHS, y.DotPos + 1, y.ProdIndex));
					}
					else //This is a Terminal
					{
						int id = AugmentedGrammar.GetTerminalIndex(t);
						if (tt[id] == nullptr)
							tt[id] = new StateNode(-1, AugmentedGrammar.T.size(), AugmentedGrammar.V.size());
						tt[id]->ProdList.push_back(CannonicalProduction(y.LHS, y.DotPos + 1, y.ProdIndex));
					}
				}
				else
					goto skip;
			}
			for (i = 0; i < AugmentedGrammar.V.size(); i++)
			{
				if (vt[i] != nullptr)
				{
					StateNode *dest = Exists(vt[i]);
					if (dest == nullptr)
					{
						dest = vt[i];
						dest->I = ++StateCount;
						States.push_back(dest);
					}
					x->OutgoingVariables[i] = dest;
				}
				else
					x->OutgoingVariables[i] = nullptr;
			}
			for (i = 0; i < AugmentedGrammar.T.size(); i++)
			{
				if (tt[i] != nullptr)
				{
					StateNode *dest = Exists(tt[i]);
					if (dest == nullptr)
					{
						dest = tt[i];
						dest->I = ++StateCount;
						States.push_back(dest);
					}
					x->OutgoingTerminals[i] = dest;
				}
				else
					x->OutgoingTerminals[i] = nullptr;
			}
			skip:;
		}
	}
	void Parse(char *x) //Going to Parse until $ or /0 is encountered
	{
		StateNode *Stack[50];
		char symbol[50];
		register int i, top=0,input=0,len=0;
		while((*(x+len))!='\0'&&(*(x+len))!='$')len++;
		Stack[0]=States[0];
		symbol[0]=' ';
		int DolInd=AugmentedGrammar.T.size()-1;
		while(Stack[top]->OutgoingTerminals[DolInd]==nullptr)
		{
			if(Stack[top]->ProdList[0].isReduced(AugmentedGrammar))
			{
				char v=Stack[top]->ProdList[0].LHS;
				top-=Stack[top]->ProdList[0].DotPos;//Neat, isn't it?
				auto y=Stack[top]->OutgoingVariables[AugmentedGrammar.GetVariableIndex(v)];
				if(y==nullptr){cout<<"\nERROR! This string is not suitable for this grammar";return;}
				else 
				{
					Stack[++top]=y;
					symbol[top]=v;
				}
			}
			else
			{
				auto y=Stack[top]->OutgoingTerminals[AugmentedGrammar.GetTerminalIndex(x[input++])];
				if(y==nullptr){cout<<"\nERROR! This string cannot be parsed to this grammar";return;}
				else 
				{
					Stack[++top]=y;
					symbol[top]=x[input];
				}
			}
			cout<<"\n---------------------------------\nInput: ";
			for(i=0;i<input;i++)cout<<x[i];
			cout<<"*";
			for(i=input;i<len;i++)cout<<x[i];
			cout<<"\nStack :";
			for(i=0;i<=top;i++){cout<<symbol[i];}
		}
		cout<<"\nThis Grammar is accepted.";
	}
};
int main()
{
	Grammar g;
	g.V = {'S', 'A', 'B'};
	g.T = {'a', 'b'};
	g.P = vector<vector<vector<char>>>(3);
	g.P[0] = vector<vector<char>>(1);
	g.P[0][0] = {'A', 'B'};
	g.P[1] = vector<vector<char>>(2);
	g.P[1][0] = {'b', 'A'};
	g.P[1][1] = {'a'};
	g.P[2] = vector<vector<char>>(1);
	g.P[2][0] = {'b', 'b'};
	LR0Parser parser(g);
	parser.Parse("bbbbabb");
	return 0;
}