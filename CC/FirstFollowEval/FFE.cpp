#include <string>
#include "..\RegularGrammer\RGE.cpp"
#include <iostream>
struct Node
{
	int i, retrig;
	Node *Next;
	Node(int x)
	{
		i = x;
		retrig = 0;
		Next = nullptr;
	}
};
void Grammer::FirstFollow(char **&First, char **&Follow)
{
	First = Follow = nullptr;
	BitArray *FirstSets = new BitArray[TerminalLength + 1], Done(VariableLength); //(VariableLength + TerminalLength + 1); //+1 for epsilon
	register int i, j, k, l, p, CurVar, sym, EpsInd = TerminalLength, containsEpsilon;
	for (i = 0; i < VariableLength; i++)
		FirstSets[i] = BitArray(TerminalLength + 1);
	Node *DepHead = new Node(0), *Script = nullptr;
	Node *DepTail = DepHead;
	for (i = 1; i < VariableLength; i++)
		DepTail = DepTail->Next = new Node(i);
	do
	{
		CurVar = DepHead->i;
		for (j = 0; j < TransitionCount[CurVar]; j++) //For Each rule defined of Variable i
		{
			k = TransitionLength[CurVar][j]; //k is the length of this transition (RHS)
			if (k == 0)
				FirstSets[CurVar].Set(EpsInd); //If length is 0, this is an epsilon transition
			else
			{
				k--;
				l = 0; //The index of the symbol in this transtion
			rep:
				sym = Transitions[CurVar][j][l];
				if (sym >= VariableLength)
					FirstSets[CurVar].Set(sym - VariableLength); //symbol is Terminal], This element is inserted as in the first set
				else if (sym != CurVar)							 //Recursive
				{
					if (Done.Get(sym)) //This Symbol is already done
					{
						for (p = 0; p < TerminalLength; p++) //Insert all elements of First[sym] in First[i] (Except epsilon)
						{
							if (FirstSets[sym].Get(p))
								FirstSets[CurVar].Set(p);
						}
						if (FirstSets[sym].Get(EpsInd)) //If first of this symbol contains epsilon
						{
							if (l == k) FirstSets[CurVar].Set(EpsInd);
							else {l++;goto rep;} //Skip for next
						}
					}
					else
						goto shift; //This is yet to be processed
				}
				else if (FirstSets[sym].Get(EpsInd))
					goto rep; //Recursive
			}
		}
		Done.Set(CurVar);
		Node *delthis = DepHead;
		DepHead = DepHead->Next;
		delete delthis;
		delthis = nullptr;
		continue;
	shift:
		DepHead->retrig++;
		if (DepHead->retrig > VariableLength)
			return; //This is fishy
		Node *t = DepHead->Next;
		DepHead->Next = nullptr;
		DepTail->Next = DepHead;
		DepHead = t;
	} while (DepHead != nullptr); //For Every Variable
	//First is now calculated. Now to Evaluate Follow
	BitArray *FollowSets = new BitArray[VariableLength];
	for (i = 0; i < VariableLength; i++)
		FollowSets[i] = BitArray(TerminalLength + 1);
	FollowSets[0].Set(EpsInd); //$ is in the Follow of S
	for (i = 0; i < VariableLength; i++)
	{
		CurVar = i;
		for (j = 0; j < TransitionCount[CurVar]; j++)
		{
			k = TransitionLength[CurVar][j];
			if (k != 0)
			{
				k--;
				for (l = 0; l <= k; l++)
				{
					sym = Transitions[CurVar][j][l];
					if (sym < VariableLength) //this sym is a Variable
					{
						if (l != k)
						{
							p=1;
						rp3:int nextsym = Transitions[CurVar][j][l+p];
							if(nextsym>=VariableLength)//this is a terminal
							{
								FollowSets[sym].Set(nextsym-VariableLength);
							}
							else if(nextsym!=sym)
							{
								for(int z=0;z<TerminalLength;z++)
								{
									if(FirstSets[nextsym].Get(z))FollowSets[sym].Set(z);
								}
								if(FirstSets[nextsym].Get(TerminalLength))
								{
									if(l+p==k)
									{
										Node*newscript=new Node(CurVar);
										newscript->retrig=sym;
										newscript->Next=Script;
										Script=newscript;
									}
									else
									{
										p++;
										goto rp3;
									}
								}
							}
						}
						else if (sym != CurVar) //Add to script
						{
							Node *newscript = new Node(CurVar); //newscript->i=CurVar;
							newscript->retrig = sym;			//FollowSet[sym] <-- Followset[CurVar]
							newscript->Next = Script;
							Script = newscript;
						}
					}
				}
			}
		}
	}
	while(Script!=nullptr)
	{
		for(p=0;p<=TerminalLength;p++)
		{
			if(FollowSets[Script->i].Get(p))FollowSets[Script->retrig].Set(p);
		}
		Node *delthis=Script;
		Script=Script->Next;
		delete delthis;
	}
	//FollowSets are also done now
	First = new char *[VariableLength];
	Follow = new char *[VariableLength];
	for (i = 0; i < VariableLength; i++)
	{
		k = 0;
		for (j = 0; j <= TerminalLength; j++)
		{
			if (FirstSets[i].Get(j))k++;
		}
		First[i] = new char[k + 1];
		First[i][k] = '\0';
		k = 0;
		for (j = 0; j < TerminalLength; j++)
		{
			if (FirstSets[i].Get(j))
			{
				First[i][k] = this->Tokens[VariableLength + j];
				k++;
			}
		}
		if (FirstSets[i].Get(EpsInd))First[i][k] = epsilon;
		k=0;
		for(j=0;j<=TerminalLength;j++)
		{
			if(FollowSets[i].Get(j))k++;
		}
		Follow[i]=new char[k+1];
		Follow[i][k]='\0';
		k=0;
		for(j=0;j<TerminalLength;j++)
		{
			if(FollowSets[i].Get(j))
			{
				Follow[i][k]=this->Tokens[VariableLength + j];
				k++;
			}
		}
		if(FollowSets[i].Get(EpsInd))Follow[i][k]='$';
	}
}
int main()
{
	register int nV, nT, i = 0, j, k, iT;
	char cc;
	int *TransitionLength;
	char *InputStr;
	char ***Transitions;
	cout << "Enter number of Variables :";
	cin >> nV;
	cout << "\nEnter number of Terminals :";
	cin >> nT;
	InputStr = new char[nV + nT + 1];
	InputStr[nV + nT] = '\0';
	cout << "\nEnter " << nV << " Variables :";
	do
	{
		cin >> cc;
		InputStr[i] = cc;
		if (InputStr[i] >= 'A' || InputStr[i] <= 'Z')
			i++;
	} while (i < nV);
	cout << "\nEnter " << nT << " Terminals :";
	do
	{
		cin >> cc;
		InputStr[i] = cc;
		if (InputStr[i] >= 'a' || InputStr[i] <= 'z')
			i++;
	} while (i < nT + nV);
	TransitionLength = new int[nV];
	Transitions = new char **[nV];
	for (i = 0; i < nV; i++)
	{
		cout << "Enter the number of Transitions for " << InputStr[i] << "\n";
		cin >> iT;
		TransitionLength[i] = iT;
		Transitions[i] = new char *[iT];
		cout << "Enter " << iT << " Transitions of " << InputStr[i] << "\n";
		for (j = 0; j < iT; j++)
		{
			cout << "\n"
				 << InputStr[i] << " -> ";
			string name;
			cin >> name;
			int pp, qq = name.length();
			Transitions[i][j] = new char[qq + 1];
			Transitions[i][j][qq] = '\0';
			for (int pp = 0; pp < qq; pp++)
				Transitions[i][j][pp] = name.at(pp);
			if (Transitions[i][j][0] == epsilon || name.length() == 0 || Transitions[i][j][0] == '$')
				Transitions[i][j] = new char[1]{0};
		}
	}
	Grammer g(nV, nT, InputStr, TransitionLength, Transitions);
	char **First, **Follow;
	g.FirstFollow(First, Follow);
	if (First != nullptr)
		cout << "\nEvaluation Complete.";
	else
	{
		cout << "\nThis Grammar cannot be processed.";
		return 0;
	}
	for (i = 0; i < nV; i++)
	{
		cout << "\nVariable " << InputStr[i] << " : First Set => {";
		k = strlength(First[i]);
		for (j = 0; j < k; j++)
			cout << First[i][j] << " ";
		cout << " }\t\tFollow Set {";
		k = strlength(Follow[i]);
		for (j = 0; j < k; j++)
			cout << Follow[i][j] << " ";
		cout << " }";
	}
	return 0;
}