using Microsoft.EntityFrameworkCore;
namespace WebSiteCore.Models
{
    public class ProductDbContext:DbContext
    {
        public const string ConnectionString=@"Server=(localdb)\MSSQLLocalDB;Database=ProductDb;Trusted_Connection=True";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }
        public DbSet<ProductModel> ProductModels {get;set;}
    }
}