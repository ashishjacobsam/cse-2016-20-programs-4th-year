﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WebSiteCore.Models;
using System.Linq;
namespace WebSiteCore.Controllers
{
    public class HomeController : Controller
    {
		internal ProductModel[] Products;
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
		public IActionResult DbTest()
		{
			using(var z=new ProductDbContext())
			{
				z.Database.EnsureCreated();
				Products=z.ProductModels.ToArray();
				z.SaveChanges();
			}
			ViewData["Products"]=Products;
			return View();
		}
		public IActionResult JsonTest()
		{
			Products=Newtonsoft.Json.JsonConvert.DeserializeObject<ProductModel[]>(System.IO.File.ReadAllText("wwwroot\\xyz.json"));
			ViewData["Products"]=Products;
			return View();
		}
		public IActionResult Forms()
		{
			return View();
		}
		public IActionResult OnFormGet(string Name, string Desc)
		{
			ViewData["Name"]=Name;
			ViewData["Desc"]=Desc;
			return View();
		}
		public IActionResult Charts()
		{
			return View();
		}
    }
}
