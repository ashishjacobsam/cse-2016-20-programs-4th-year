﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyPage.aspx.cs" Inherits="NewWebForms.MyPage" %>
<%@ Register Src="~/Controllers/CustomController.ascx" TagPrefix="CustomControllers" TagName="CustomLocal" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Enter Your Name" BackColor="#CC0066" BorderColor="#FF6699" ForeColor="#CCFF99"></asp:Label>
        </div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br />
        <asp:Label ID="Label2" runat="server" Text="Select your city"></asp:Label><br />
        <asp:ListBox ID="ListBox1" runat="server" Height="93px" Width="341px">
            <asp:ListItem Selected="True">Mumbai</asp:ListItem>
            <asp:ListItem>Delhi</asp:ListItem>
            <asp:ListItem>Kochin</asp:ListItem>
            <asp:ListItem>Bangalore</asp:ListItem>
        </asp:ListBox><br />
        <asp:Label ID="Label3" runat="server" Text="Enter Your Gender"></asp:Label>
        <CustomControllers:CustomLocal runat="server" />
        <asp:RadioButtonList ID="radiobuttons" runat="server">
            <asp:ListItem Selected="True">Male</asp:ListItem>
            <asp:ListItem>Female</asp:ListItem>
        </asp:RadioButtonList>
        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
            <asp:ListItem>C#</asp:ListItem>
            <asp:ListItem>ASP .NET</asp:ListItem>
        </asp:CheckBoxList>
        <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" Width="169px" />
        <p>

            <% Response.Write(Session["Name"]);%>
        </p>
        <CustomControllers:CustomLocal runat="server" />
        <asp:Panel ID="Panel1" runat="server" Height="63px"/>
        <CustomControllers:CustomGlobal runat="server" />
    </form>
</body>
</html>
