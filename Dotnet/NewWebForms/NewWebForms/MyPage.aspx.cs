﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NewWebForms
{
    public partial class MyPage : System.Web.UI.Page
    {
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Write($"Hello {TextBox1.Text} </br>");
            Session["Name"] = TextBox1.Text;
            Response.Write($"You live in {ListBox1.SelectedValue}");
            Label1.Visible = false;
            Label2.Visible = false;
            Label3.Visible = false;
            TextBox1.Visible = false;
            ListBox1.Visible = false;
            Button1.Visible = false;
            radiobuttons.Visible = false;
            CheckBoxList1.Visible = false;
        }
    }
}