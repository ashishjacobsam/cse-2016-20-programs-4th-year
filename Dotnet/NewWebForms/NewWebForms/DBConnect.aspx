﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DBConnect.aspx.cs" Inherits="NewWebForms.DBConnect" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table border="1" style="height: 125px; width: 393px">
            <tr>
                <th>Name</th>
                <th>Price</th>
            </tr>
            <%

                string connectstr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Test1;Integrated Security=True";
                var con = new System.Data.SqlClient.SqlConnection(connectstr);
                con.Open();
                var sqlCommand = new System.Data.SqlClient.SqlCommand("Select * From MyTable", con);
                var reader = sqlCommand.ExecuteReader();
                while(reader.Read())
                { %><tr>
                    <td style="font-family: Arial; font-size: 12px; font-weight: bold; font-style: normal; font-variant: normal; text-transform: capitalize; color: #FF0000"><%=reader.GetValue(1)%></td>
                    <td style="font-family: Arial; font-weight: bold; font-style: normal; font-variant: normal; text-transform: lowercase; color: #0000FF"><%=reader.GetValue(2)%></td>
                </tr>
            <%} %>
        </table>
    </form>
</body>
</html>
