typedef unsigned int uint;
class ArrayGraph
{
	uint **array;
	uint VerticesCount;
public:
	ArrayGraph(uint v)
	{
		VerticesCount=v;
		array=new uint*[VerticesCount];
		for(uint i=0;i<VerticesCount;i++)array[i]=new uint[VerticesCount]();
	}
	~ArrayGraph()
	{
		for(uint i=0;i<VerticesCount;i++)
		{
			if(array[i])
			{
				delete[] array[i];
				array[i]=nullptr;
			}
		}
		if(array&&array!=nullptr)
			delete[] array;
		array=nullptr;
	}
	int GetVerticesCount(){return VerticesCount;}
	void Set(uint i,uint j,uint value)
	{
		if(i<VerticesCount&&j<VerticesCount&&i!=j)array[i][j]=value;
	}
	void Remove(uint i,uint j)
	{
		if(i<VerticesCount&&j<VerticesCount&&i!=j)array[i][j]=0;
	}
	uint Get(uint i,uint j)
	{
		if(i<VerticesCount&&j<VerticesCount)return array[i][j];
		else return 0;
	}
	void GetOutgoingVertices(uint source,uint *&Neighbours, uint *&weights, int &Outdegree)
	{
		int i,j;
		Outdegree=0;
		for(i=0;i<VerticesCount;i++)
		{
			if(array[source][i]!=0)Outdegree++;
		}
		if(Outdegree==0)
		{
			Neighbours=weights=nullptr;
			return;
		}
		Neighbours=new uint[Outdegree];
		weights=new uint[Outdegree];
		for(i=j=0;i<VerticesCount;i++)
		{
			if(array[source][i]!=0)
			{
				Neighbours[j]=i;
				weights[j++]=array[source][i];
			}
		}
	}
	void GetIncomingVertices(uint source,uint *&Neighbours, uint *&weights, int &Outdegree)
	{
		int i,j;
		Outdegree=0;
		for(i=0;i<VerticesCount;i++)
		{
			if(array[i][source]!=0)Outdegree++;
		}
		if(Outdegree==0)
		{
			Neighbours=weights=nullptr;
			return;//Stop Prematurely
		}
		Neighbours=new uint[Outdegree];
		weights=new uint[Outdegree];
		for(i=j=0;i<VerticesCount&&j<Outdegree;i++)
		{
			if(array[i][source]!=0)
			{
				Neighbours[j]=i;
				weights[j++]=array[i][source];
			}
		}
	}
};