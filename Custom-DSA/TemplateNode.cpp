/*
NOTICE: THIS FOLDER HAS NOTHING TO DO WITH LAB. IF YOU CAME HERE ONLY FOR LAB FILES, YOU CAME TO THE WRONG FOLDER

Here you will some implementation of Data structures and graphs if you want to study a bit more on Data Structures and Algorithms
*/
#define null nullptr	//If this does not work in the Dinasaur-era Borland Compiler, just change nullptr to NULL
template<class T>
struct Node
{
	Node<T> *Next,*Prev;
	T data;
	int count;
	Node(T value){data=value;Next=Prev=null;}
};
template<class T>
class Queue
{
	public:
	Node<T> *Head,*Tail;
	int count;
	Queue()
	{
		count=0;
		Head=Tail=null;
	}
	void Push(T val)
	{
		Node<T>*x=new Node<T>(val);
		if(Head==nullptr)
			Head=Tail=x;
		else
		{
			x->Prev=Tail;
			Tail->Next=x;
			Tail=x;
		}
		count++;
	}
	bool Pop()
	{
		Node<T>*x=Head;
		if(Head==null)return false;
		else if(Head==Tail)
			Head=Tail=null;
		else 
		{
			Head=Head->next;
			Head->Prev=null;
		}
		delete x;
		count--;
		return true;
	}
	int GetCount()
	{
		return count;
	}
	T* GetArray()
	{
		T *x;
		x=new T[count];
		Node<T> *ii=Head;
		for(int i=0;i<count;i++,ii=ii->Next)
		{
			x[i]=ii->data;
		}
		return x;
	}
};