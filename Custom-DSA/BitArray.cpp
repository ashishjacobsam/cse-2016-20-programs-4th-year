/*
NOTICE: THIS FOLDER HAS NOTHING TO DO WITH LAB. IF YOU CAME HERE ONLY FOR LAB FILES, YOU CAME TO THE WRONG FOLDER

Here you will some implementation of Data structures and graphs if you want to study a bit more on Data Structures and Algorithms
*/


/*Returns the size of int for platform dependent support. 
Since Borland compilers were running on 16-bit processors and unless you are using a processor before Intel 80386,
the below macro should do the job for you (supposedly, anyway)
*/
typedef unsigned int uint;
uint isize=8*(sizeof(int));
//A 1D array of bits
class BitArray
{
public:
	uint size;
	uint *Array;
	BitArray()
	{
		size=0;
		Array=nullptr;
	}
	BitArray(uint s)
	{
		if (s < 2)
			s = 1;
		size = s%isize==0?s/isize:1+(s/isize);
		Array = new uint[size]();
	}
	int BitsCount() { return size * isize; }
	void Set(uint loc)
	{
		if (loc > BitsCount())return;
		Array[loc / isize] |= (1 << (loc % isize));
	}
	void Reset(uint loc)
	{
		if (loc > BitsCount())return;
		Array[loc / isize] &= ~(1 << (loc % isize));
	}
	int Get(unsigned int loc)
	{
		if (loc > BitsCount())return -1;
		return ( ((Array[loc/isize])&(1<<(loc%isize))) != 0)?1:0;
	}
	void SetAll(){for(register uint i=0;i<size;i++)Array[i]=1;}
	void ResetAll(){for(register uint i=0;i<size;i++)Array[i]=0;}
};
class SquareBitArray
{
public:
	BitArray bits;
	uint Side;
	uint Index(uint r,uint c)
	{
		return c+(Side*r);
	}
	SquareBitArray(){Side=0;}
	SquareBitArray(uint x)
	{
		Side=x;
		unsigned long b=x*x;
		if(b%isize==0)b=b/isize;
		else b=1+(b/isize);
		bits=BitArray(b);
	}
	uint BitsCount(){return bits.BitsCount();}
	void Set(uint x,uint y)
	{
		if(x>=Side||y>=Side)return;
		bits.Set(Index(x,y));
	}
	void Reset(uint x,uint y)
	{
		if(x>=Side||y>=Side)return;
		bits.Reset(Index(x,y));
	}
	int Get(uint x,uint y)
	{
		if(x>=Side||y>=Side)return -1;
		else return bits.Get(Index(x,y));
	}
};