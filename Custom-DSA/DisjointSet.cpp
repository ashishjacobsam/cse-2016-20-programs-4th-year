typedef unsigned int uint;
class DisjointSets
{
	uint *Parent,*Rank;
	uint Size;
public:
	DisjointSets(uint size)
	{
		Parent=new uint[size];
		Rank=new uint[size];
		Size=size;
		Reset();
	}
	void Reset()
	{
		for(register uint i=0;i<Size;i++)
		{
			Parent[i]=i;
			Rank[i]=0;
		}
	}
	uint GetRoot(uint i)
	{
		if(i>=Size)return i;
		while(i!=Parent[i])i=Parent[i];
		return i;
	}
	void Union(uint i,uint j)
	{
		i=GetRoot(i);
		j=GetRoot(j);
		if(i==j)return;
		else if(Rank[i]>Rank[j])//i already has some child and thus has a higher rank
			Parent[j]=i;
		else//j can either be of equal or greater rank
		{
			Parent[i]=j;
			if(Rank[j]==Rank[i])//If both are of same rank, then it no longer the case now
				Rank[j]++;
		}
	}
	int CheckUnion(uint i,uint j)
	{
		return GetRoot(i)==GetRoot(j);
	}
};