/*
NOTICE: THIS FOLDER HAS NOTHING TO DO WITH LAB. IF YOU CAME HERE ONLY FOR LAB FILES, YOU CAME TO THE WRONG FOLDER

Here you will some implementation of Data structures and graphs if you want to study a bit more on Data Structures and Algorithms
*/

//Rename the typedef to whatever you want
typedef int Value;

//Uncomment the previous macro to enable NULL in Borland
//#define nullptr NULL

//A single Node in Stack
struct Node
{
	Value value;
	Node *Next;
};

Node* Create(Value v)
{
	Node*temp=new Node;
	temp->value=v;
}

class Stack
{
	Node*Top=nullptr;
	int count=0;
	public:
	void Push(Value v)
	{
		Node*t=Create(v);
		t->Next=Top;
		count++;
	}
	bool Pop()
	{
		if(count==0)return false;
		Node *delthis=Top;
		Top=Top->Next;
		delete delthis;
		return true;
	}
	Value getTop(){return Top->value;}
	bool isEmpty(){return count==0;}
	int Count(){return count;}
};