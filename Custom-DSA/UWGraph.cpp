//This is Undirected Weighted Graph (UWGraph)
typedef unsigned int uint;
class UWEdge
{
public:
	UWEdge(uint v1,uint v2,uint w)
	{
		Vertex1=v1;
		Vertex2=v2;
		weight=w;
		Next=Prev=nullptr;
	}
	uint Vertex1,Vertex2,weight;
	UWEdge *Prev,*Next;//Part of Stack
	bool IsSimilar(uint v1,uint v2)//You can replace bool with int
	{
		return (v1==Vertex1&&v2==Vertex2)||(v1==Vertex2&&v2==Vertex1);
	}
	bool ContainsVertex(int CheckVertex)
	{
		return Vertex1==CheckVertex||Vertex2==CheckVertex;
	}
};
class UWGraph
{
	uint VertexCount,EdgeCount;
	UWEdge *Head,*Tail;
	UWEdge* FindEdge(uint v1,uint v2)
	{
		UWEdge *a=Head;
		while(a!=nullptr&&(!(a->IsSimilar(v1,v2))))a=a->Next;
		return a;
	}
public:
	UWGraph(uint v)
	{
		EdgeCount=0;
		VertexCount=v;
		Head=Tail=nullptr;
	}
	void SetEdge(uint v1,uint v2,uint w)
	{
		if(v1>=VertexCount||v2>=VertexCount||w==0)
		return;
		UWEdge *edge=FindEdge(v1,v2);
		if(edge==nullptr)//Add edge
		{
			edge=new UWEdge(v1,v2,w);
			if(Head==nullptr)Head=Tail=edge;
			else
			{
				edge->Prev=Tail;
				Tail=Tail->Next=edge;
			}
			EdgeCount++;
		}
		else if(w==0)//Remove this edge
		{
			if(Head==edge)Head=Head->Next;else edge->Prev->Next=edge->Next;
			if(Tail==edge)Tail=Tail->Prev;else edge->Next->Prev=edge->Prev;
			delete edge;//These 2 conditions take care of everything
			EdgeCount--;
		}
		else edge->weight=w;//Edit edge
	}
	uint GetEdge(uint v1,uint v2)
	{
		UWEdge *edge=FindEdge(v1,v2);
		if(edge==nullptr)return 0;
		else return edge->weight;
	}
	uint VerticesCount(){return VertexCount;}
	uint EdgesCount(){return EdgeCount;}
	UWEdge* GetHead(){return Head;}
};