typedef unsigned int uint;
class Node
{
public:
	uint id;
	uint ChildCount;
	Node **Children;//An array of pointers and not a 2D array
	Node(uint n)
	{
		id=n;
		ChildCount=0;
		Children=nullptr;
	}
	void SetChildren(uint x)
	{
		ChildCount=x;
		Children=new Node*[x];
	}
	~Node()
	{
		if(Children!=nullptr)delete[] Children;//Rule of Thumb: Kill Children before Parent [It is advised that this rule is not taken out of context]
	}
};

