/*
NOTICE: THIS FOLDER HAS NOTHING TO DO WITH LAB. IF YOU CAME HERE ONLY FOR LAB FILES, YOU CAME TO THE WRONG FOLDER

Here you will some implementation of Data structures and graphs if you want to study a bit more on Data Structures and Algorithms
*/

//Rename to whatever value you need
typedef unsigned int Value;
//A node in a binary tree
class BinaryTreeNode
{
public:
	Value value;
	unsigned int ChildrenCount,Height;
	BinaryTreeNode *LeftChild, *RightChild;
	BinaryTreeNode(Value v)
	{
		value = v;
		ChildrenCount = Height = 0;
		LeftChild = RightChild = nullptr;
	}
	~BinaryTreeNode() //Rule of thumb : Kill the children before their parents (Yes, I love jokes like these)
	{
		delete LeftChild;
		delete RightChild;
	}
	void SetHeight()
	{
		int height=0;
	}
};
//C++ implementation of a Binary Search Tree
class BST
{
	BinaryTreeNode *Root = nullptr;
	BinaryTreeNode *Add(BinaryTreeNode *NodeToAdd,BinaryTreeNode *RootNode)
	{
		BinaryTreeNode *subTree = ((NodeToAdd->value) < (RootNode->value)) ? RootNode->LeftChild : RootNode->RightChild;
		if (subTree)subTree = Add(NodeToAdd,subTree);
		else subTree = NodeToAdd;
		RootNode->ChildrenCount++;
	}
	BinaryTreeNode *Find(Value v) //Non Recusive Binary Search
	{
		BinaryTreeNode *NowSearching = Root;
		while (NowSearching)
		{
			if (v == NowSearching->value)
				return NowSearching; //This is the node with the correct value
			else if (v < NowSearching->value)
				NowSearching = NowSearching->LeftChild;
			else
				NowSearching = NowSearching->RightChild;
		}
		return nullptr; //Node not found
	}
\
public:
	BST() { Root = nullptr; }
	BinaryTreeNode *Add(Value v)
	{
		return Add(v,Root);
	}
	BinaryTreeNode *Add(Value v, BinaryTreeNode *RootNode)
	{
		BinaryTreeNode *ValueNode = new BinaryTreeNode(v);
		if (RootNode == nullptr)
			return ValueNode;
		else
			return Add(ValueNode,RootNode);
	}
};
