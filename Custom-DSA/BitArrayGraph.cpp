/*
NOTICE: THIS FOLDER HAS NOTHING TO DO WITH LAB. IF YOU CAME HERE ONLY FOR LAB FILES, YOU CAME TO THE WRONG FOLDER

Here you will some implementation of Data structures and graphs if you want to study a bit more on Data Structures and Algorithms
*/
#include "BitArray.cpp"

typedef unsigned int uint;

//This is a custom implementation of non-weighted Directed graph based on the 2D Bitarray
//If you want this Undirected, for every Set() and Reset method between e1 -> e2, invoke for e2 -> e1 as well 
class Graph
{
public:
	uint VertexCount;
	SquareBitArray Edges;
	Graph(uint size=1)
	{
		VertexCount=size;
		Edges=SquareBitArray(VertexCount);
	}
	void Set(uint e1,uint e2)
	{
		Edges.Set(e1,e2);
	}
	void Reset(uint e1,uint e2)
	{
		Edges.Reset(e1,e2);
	}
	int Get(uint e1,uint e2){return Edges.Get(e1,e2);}
	uint GetVerticesCount(){return VertexCount;}
};