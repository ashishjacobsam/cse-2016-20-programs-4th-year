typedef unsigned int uint;
struct Node
{
	uint Vertex;
	uint Weight;
	Node *Next,*Prev;
};
Node*CreateNode(uint Vval=0,uint Wval=0)
{
	Node *newNode=new Node;
	newNode->Next=newNode->Prev=nullptr;
	newNode->Vertex=Vval;
	newNode->Weight=Wval;
}
struct NodeGraph
{
	uint VertexCount;	//No. of Vertex
	//Seraches for an edge between i and j and returns the pointer if it exists, some clever data exchange otherwise
	Node* Search(uint i,uint j)
	{
		Node* x=Edges[i];
		if(x==nullptr)return x;
		while(x->Next!=nullptr&&x->Vertex!=j)x=x->Next;
		return x;
	}
public:
	Node **Edges;		//Array of Edge-Pointers of each vertex
	NodeGraph(uint Vertexes, int Directed=0)
	{
		VertexCount=Vertexes;
		Edges=new Node*[VertexCount];
		for(int i=0;i<VertexCount;i++)Edges[i]=nullptr;
	}
	//Sets an edge from i to j : i -> j
	//If Weight=0, Removes the edge (Can't have an edge of weight 0 now, alright?)
	int Set(uint i,uint j,int weight)
	{
		Node *alreadyExisting=Search(i,j);
		if(alreadyExisting==nullptr)//This is the first edge of the graph
		{
			if(weight!=0)Edges[i]=CreateNode(j,weight);
			return 1;
		}
		else if(alreadyExisting->Vertex!=j)//This Vertex already have edges. Add this new edge on top of it
		{
			if(weight!=0)
			{
				Node *newNode=CreateNode(j,weight);
				alreadyExisting->Next=newNode;
				newNode->Prev=alreadyExisting;
			}
		}
		else//This Edge already exists. In that case, overwrite its weight or Remove 
		{
			if(weight==0)//Remove
			{
				if(alreadyExisting->Prev!=nullptr)alreadyExisting->Prev->Next=alreadyExisting->Next;
				if(alreadyExisting->Next!=nullptr)alreadyExisting->Next->Prev=alreadyExisting->Prev;
				delete alreadyExisting;
			}
			else//Overwrite
				alreadyExisting->Weight=weight;
		}
	}
	//Basically makes a two-way edge i->j and j->i
	//
	//Please do not Mix Undirected and Directed weight setters for your own good
	int SetUndirected(uint i,uint j,uint weight)
	{
		Set(i,j,weight);
		Set(j,i,weight);//Genius, AmIRite?
	}
	//Returns the weight of the edge i->j , if it exists. Otherwise returns 0; 
	uint Get(uint i,uint j)
	{
		Node *alreadyExisting=Search(i,j);
		if(alreadyExisting==nullptr||alreadyExisting->Vertex!=j)return 0;
		else return alreadyExisting->Weight;
	}
};
