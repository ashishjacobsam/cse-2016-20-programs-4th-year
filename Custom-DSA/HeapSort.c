/*
NOTICE: THIS FOLDER HAS NOTHING TO DO WITH LAB. IF YOU CAME HERE ONLY FOR LAB FILES, YOU CAME TO THE WRONG FOLDER

Here you will some implementation of Data structures and graphs if you want to study a bit more on Data Structures and Algorithms
*/

//Builds the array-based heap. Performance = O(log n)
void buildMaxHeap(int arr[], int n)
{
	for (int i = 1; i < n; i++)
	{// if child is bigger than parent
		if (arr[i] > arr[(i - 1) / 2])
		{
			int j = i;// swap child and parent until parent is smaller
			while (arr[j] > arr[(j - 1) / 2])
			{
				swap(arr[j], arr[(j - 1) / 2]);
				j = (j - 1) / 2;
			}
		}
	}
}
//IMHO, The best array-based n.log(n) performance sorting function out there for grabs
void heapSort(int arr[], int n)
{
	buildMaxHeap(arr, n);
	for (int i = n - 1; i > 0; i--)
	{// swap value of first indexed with last indexed
		swap(arr[0], arr[i]);// maintaining heap property after each swapping
		int j = 0, index;
		do
		{
			index = (2 * j + 1); 
			if (arr[index] < arr[index + 1] &&index < (i - 1))// if left child is smaller
				index++;
			if (arr[j] < arr[index] && index < i)// if parent is smaller than child then swap parent with child
				swap(arr[j], arr[index]);
			j = index;
		} while (index < i);
	}
}