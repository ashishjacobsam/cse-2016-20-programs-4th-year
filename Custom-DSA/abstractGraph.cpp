/*
NOTICE: THIS FOLDER HAS NOTHING TO DO WITH LAB. IF YOU CAME HERE ONLY FOR LAB FILES, YOU CAME TO THE WRONG FOLDER

Here you will some implementation of Data structures and graphs if you want to study a bit more on Data Structures and Algorithms
*/



template <class V,class E>
class AbstractGraph
{
	public:
	virtual unsigned int VertexCount()=0;
	virtual void Set(V v1,V v2,E value)=0;
	virtual E Get(V v1,V v2)=0;
};